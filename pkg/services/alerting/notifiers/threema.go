package notifiers

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/grafana/grafana/pkg/bus"
	"github.com/grafana/grafana/pkg/infra/log"
	"github.com/grafana/grafana/pkg/models"
	"github.com/grafana/grafana/pkg/services/alerting"
)

var (
	threemaGwBaseURL = "https://msgapi.threema.ch/%s"
)

func init() {
	alerting.RegisterNotifier(&alerting.NotifierPlugin{
		Type:        "threema",
		Name:        "Threema Gateway",
		Description: "使用Threema网关向Threema发送通知",
		Factory:     NewThreemaNotifier,
		OptionsTemplate: `
      <h3 class="page-heading">Threema 网关设置</h3>
      <p>
        可以为“基本”类型的任何Threema网关ID配置通知。当前不支持端到端标识。
      </p>
      <p>
        Threema网关ID可以设置在
        <a href="https://gateway.threema.ch/" target="_blank" rel="noopener noreferrer">https://gateway.threema.ch/</a>.
      </p>
      <div class="gf-form">
        <span class="gf-form-label width-14">网关ID</span>
        <input type="text" required maxlength="8" pattern="\*[0-9A-Z]{7}"
          class="gf-form-input max-width-14"
          ng-model="ctrl.model.settings.gateway_id"
          placeholder="*3MAGWID">
        </input>
        <info-popover mode="right-normal">
          8个字符的Threema网关ID（以*开头）
        </info-popover>
      </div>
      <div class="gf-form">
        <span class="gf-form-label width-14">收件人ID</span>
        <input type="text" required maxlength="8" pattern="[0-9A-Z]{8}"
          class="gf-form-input max-width-14"
          ng-model="ctrl.model.settings.recipient_id"
          placeholder="YOUR3MID">
        </input>
        <info-popover mode="right-normal">
          应接收告警的8字符Threema ID
        </info-popover>
      </div>
      <div class="gf-form">
        <span class="gf-form-label width-14">API密码</span>
        <input type="text" required
          class="gf-form-input max-width-24"
          ng-model="ctrl.model.settings.api_secret">
        </input>
        <info-popover mode="right-normal">
          您的Threema 网关API密码
        </info-popover>
      </div>
    `,
	})

}

// ThreemaNotifier is responsible for sending
// alert notifications to Threema.
type ThreemaNotifier struct {
	NotifierBase
	GatewayID   string
	RecipientID string
	APISecret   string
	log         log.Logger
}

// NewThreemaNotifier is the constructor for the Threema notifer
func NewThreemaNotifier(model *models.AlertNotification) (alerting.Notifier, error) {
	if model.Settings == nil {
		return nil, alerting.ValidationError{Reason: "No Settings Supplied"}
	}

	gatewayID := model.Settings.Get("gateway_id").MustString()
	recipientID := model.Settings.Get("recipient_id").MustString()
	apiSecret := model.Settings.Get("api_secret").MustString()

	// Validation
	if gatewayID == "" {
		return nil, alerting.ValidationError{Reason: "Could not find Threema Gateway ID in settings"}
	}
	if !strings.HasPrefix(gatewayID, "*") {
		return nil, alerting.ValidationError{Reason: "Invalid Threema Gateway ID: Must start with a *"}
	}
	if len(gatewayID) != 8 {
		return nil, alerting.ValidationError{Reason: "Invalid Threema Gateway ID: Must be 8 characters long"}
	}
	if recipientID == "" {
		return nil, alerting.ValidationError{Reason: "Could not find Threema Recipient ID in settings"}
	}
	if len(recipientID) != 8 {
		return nil, alerting.ValidationError{Reason: "Invalid Threema Recipient ID: Must be 8 characters long"}
	}
	if apiSecret == "" {
		return nil, alerting.ValidationError{Reason: "Could not find Threema API secret in settings"}
	}

	return &ThreemaNotifier{
		NotifierBase: NewNotifierBase(model),
		GatewayID:    gatewayID,
		RecipientID:  recipientID,
		APISecret:    apiSecret,
		log:          log.New("alerting.notifier.threema"),
	}, nil
}

// Notify send an alert notification to Threema
func (notifier *ThreemaNotifier) Notify(evalContext *alerting.EvalContext) error {
	notifier.log.Info("Sending alert notification from", "threema_id", notifier.GatewayID)
	notifier.log.Info("Sending alert notification to", "threema_id", notifier.RecipientID)

	// Set up basic API request data
	data := url.Values{}
	data.Set("from", notifier.GatewayID)
	data.Set("to", notifier.RecipientID)
	data.Set("secret", notifier.APISecret)

	// Determine emoji
	stateEmoji := ""
	switch evalContext.Rule.State {
	case models.AlertStateOK:
		stateEmoji = "\u2705 " // Check Mark Button
	case models.AlertStateNoData:
		stateEmoji = "\u2753\uFE0F " // Question Mark
	case models.AlertStateAlerting:
		stateEmoji = "\u26A0\uFE0F " // Warning sign
	}

	// Build message
	message := fmt.Sprintf("%s%s\n\n*State:* %s\n*Message:* %s\n",
		stateEmoji, evalContext.GetNotificationTitle(),
		evalContext.Rule.Name, evalContext.Rule.Message)
	ruleURL, err := evalContext.GetRuleURL()
	if err == nil {
		message = message + fmt.Sprintf("*URL:* %s\n", ruleURL)
	}
	if evalContext.ImagePublicURL != "" {
		message = message + fmt.Sprintf("*Image:* %s\n", evalContext.ImagePublicURL)
	}
	data.Set("text", message)

	// Prepare and send request
	url := fmt.Sprintf(threemaGwBaseURL, "send_simple")
	body := data.Encode()
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	cmd := &models.SendWebhookSync{
		Url:        url,
		Body:       body,
		HttpMethod: "POST",
		HttpHeader: headers,
	}
	if err := bus.DispatchCtx(evalContext.Ctx, cmd); err != nil {
		notifier.log.Error("Failed to send webhook", "error", err, "webhook", notifier.Name)
		return err
	}

	return nil
}
