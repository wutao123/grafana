package migrations

import (
	. "github.com/grafana/grafana/pkg/services/sqlstore/migrator"
)

func addQuotaMigration(mg *Migrator) {

	var quotaV1 = Table{
		Name: "quota",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "组织id", Type: DB_BigInt, Nullable: true},
			{Name: "用户id", Type: DB_BigInt, Nullable: true},
			{Name: "目标", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "限制", Type: DB_BigInt, Nullable: false},
			{Name: "创建", Type: DB_DateTime, Nullable: false},
			{Name: "更新", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "user_id", "target"}, Type: UniqueIndex},
		},
	}
	mg.AddMigration("create quota table v1", NewAddTableMigration(quotaV1))

	//-------  indexes ------------------
	addTableIndicesMigrations(mg, "v1", quotaV1)

	mg.AddMigration("Update quota table charset", NewTableCharsetMigration("quota", []*Column{
		{Name: "target", Type: DB_NVarchar, Length: 190, Nullable: false},
	}))
}
