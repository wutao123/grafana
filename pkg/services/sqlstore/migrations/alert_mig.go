package migrations

import (
	. "github.com/grafana/grafana/pkg/services/sqlstore/migrator"
)

func addAlertMigrations(mg *Migrator) {

	alertV1 := Table{
		Name: "告警",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "版本", Type: DB_BigInt, Nullable: false},
			{Name: "仪表盘id", Type: DB_BigInt, Nullable: false},
			{Name: "面板id", Type: DB_BigInt, Nullable: false},
			{Name: "组织id", Type: DB_BigInt, Nullable: false},
			{Name: "名称", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "消息", Type: DB_Text, Nullable: false},
			{Name: "状态", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "设置", Type: DB_Text, Nullable: false},
			{Name: "频率", Type: DB_BigInt, Nullable: false},
			{Name: "处理者", Type: DB_BigInt, Nullable: false},
			{Name: "严重程度", Type: DB_Text, Nullable: false},
			{Name: "压制", Type: DB_Bool, Nullable: false},
			{Name: "执行错误", Type: DB_Text, Nullable: false},
			{Name: "评估数据", Type: DB_Text, Nullable: true},
			{Name: "评估日期", Type: DB_DateTime, Nullable: true},
			{Name: "新状态日期", Type: DB_DateTime, Nullable: false},
			{Name: "状态更改", Type: DB_Int, Nullable: false},
			{Name: "创建", Type: DB_DateTime, Nullable: false},
			{Name: "更新", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "id"}, Type: IndexType},
			{Cols: []string{"state"}, Type: IndexType},
			{Cols: []string{"dashboard_id"}, Type: IndexType},
		},
	}

	// create table
	mg.AddMigration("create alert table v1", NewAddTableMigration(alertV1))

	// create indices
	mg.AddMigration("add index alert org_id & id ", NewAddIndexMigration(alertV1, alertV1.Indices[0]))
	mg.AddMigration("add index alert state", NewAddIndexMigration(alertV1, alertV1.Indices[1]))
	mg.AddMigration("add index alert dashboard_id", NewAddIndexMigration(alertV1, alertV1.Indices[2]))

	alertRuleTagTable := Table{
		Name: "告警规则标签",
		Columns: []*Column{
			{Name: "告警id", Type: DB_BigInt, Nullable: false},
			{Name: "标签id", Type: DB_BigInt, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"alert_id", "tag_id"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("Create alert_rule_tag table v1", NewAddTableMigration(alertRuleTagTable))
	mg.AddMigration("Add unique index alert_rule_tag.alert_id_tag_id", NewAddIndexMigration(alertRuleTagTable, alertRuleTagTable.Indices[0]))

	alert_notification := Table{
		Name: "告警通知",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "组织id", Type: DB_BigInt, Nullable: false},
			{Name: "名称", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "类型", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "设置", Type: DB_Text, Nullable: false},
			{Name: "创建", Type: DB_DateTime, Nullable: false},
			{Name: "更新", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "name"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("create alert_notification table v1", NewAddTableMigration(alert_notification))
	mg.AddMigration("Add column is_default", NewAddColumnMigration(alert_notification, &Column{
		Name: "is_default", Type: DB_Bool, Nullable: false, Default: "0",
	}))
	mg.AddMigration("Add column frequency", NewAddColumnMigration(alert_notification, &Column{
		Name: "frequency", Type: DB_BigInt, Nullable: true,
	}))
	mg.AddMigration("Add column send_reminder", NewAddColumnMigration(alert_notification, &Column{
		Name: "send_reminder", Type: DB_Bool, Nullable: true, Default: "0",
	}))
	mg.AddMigration("Add column disable_resolve_message", NewAddColumnMigration(alert_notification, &Column{
		Name: "disable_resolve_message", Type: DB_Bool, Nullable: false, Default: "0",
	}))

	mg.AddMigration("add index alert_notification org_id & name", NewAddIndexMigration(alert_notification, alert_notification.Indices[0]))

	mg.AddMigration("Update alert table charset", NewTableCharsetMigration("alert", []*Column{
		{Name: "name", Type: DB_NVarchar, Length: 255, Nullable: false},
		{Name: "message", Type: DB_Text, Nullable: false},
		{Name: "state", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "settings", Type: DB_Text, Nullable: false},
		{Name: "severity", Type: DB_Text, Nullable: false},
		{Name: "execution_error", Type: DB_Text, Nullable: false},
		{Name: "eval_data", Type: DB_Text, Nullable: true},
	}))

	mg.AddMigration("Update alert_notification table charset", NewTableCharsetMigration("alert_notification", []*Column{
		{Name: "name", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "type", Type: DB_NVarchar, Length: 255, Nullable: false},
		{Name: "settings", Type: DB_Text, Nullable: false},
	}))

	notification_journal := Table{
		Name: "alert_notification_journal",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "组织id", Type: DB_BigInt, Nullable: false},
			{Name: "告警id", Type: DB_BigInt, Nullable: false},
			{Name: "通知id", Type: DB_BigInt, Nullable: false},
			{Name: "发送时间", Type: DB_BigInt, Nullable: false},
			{Name: "成功", Type: DB_Bool, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "alert_id", "notifier_id"}, Type: IndexType},
		},
	}

	mg.AddMigration("create notification_journal table v1", NewAddTableMigration(notification_journal))
	mg.AddMigration("add index notification_journal org_id & alert_id & notifier_id", NewAddIndexMigration(notification_journal, notification_journal.Indices[0]))

	mg.AddMigration("drop alert_notification_journal", NewDropTableMigration("alert_notification_journal"))

	alert_notification_state := Table{
		Name: "alert_notification_state",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "组织id", Type: DB_BigInt, Nullable: false},
			{Name: "告警id", Type: DB_BigInt, Nullable: false},
			{Name: "通知者id", Type: DB_BigInt, Nullable: false},
			{Name: "状态", Type: DB_NVarchar, Length: 50, Nullable: false},
			{Name: "版本", Type: DB_BigInt, Nullable: false},
			{Name: "更新时间", Type: DB_BigInt, Nullable: false},
			{Name: "警报规则状态更新版本", Type: DB_BigInt, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "alert_id", "notifier_id"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("create alert_notification_state table v1", NewAddTableMigration(alert_notification_state))
	mg.AddMigration("add index alert_notification_state org_id & alert_id & notifier_id",
		NewAddIndexMigration(alert_notification_state, alert_notification_state.Indices[0]))

	mg.AddMigration("Add for to alert table", NewAddColumnMigration(alertV1, &Column{
		Name: "for", Type: DB_BigInt, Nullable: true,
	}))

	mg.AddMigration("Add column uid in alert_notification", NewAddColumnMigration(alert_notification, &Column{
		Name: "uid", Type: DB_NVarchar, Length: 40, Nullable: true,
	}))

	mg.AddMigration("Update uid column values in alert_notification", new(RawSqlMigration).
		Sqlite("UPDATE alert_notification SET uid=printf('%09d',id) WHERE uid IS NULL;").
		Postgres("UPDATE alert_notification SET uid=lpad('' || id::text,9,'0') WHERE uid IS NULL;").
		Mysql("UPDATE alert_notification SET uid=lpad(id,9,'0') WHERE uid IS NULL;"))

	mg.AddMigration("Add unique index alert_notification_org_id_uid", NewAddIndexMigration(alert_notification, &Index{
		Cols: []string{"org_id", "uid"}, Type: UniqueIndex,
	}))

	mg.AddMigration("Remove unique index org_id_name", NewDropIndexMigration(alert_notification, &Index{
		Cols: []string{"org_id", "name"}, Type: UniqueIndex,
	}))
}
