package migrations

import . "github.com/grafana/grafana/pkg/services/sqlstore/migrator"

func addDashboardSnapshotMigrations(mg *Migrator) {
	snapshotV4 := Table{
		Name: "dashboard_snapshot",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "名称", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "密钥", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "仪表盘", Type: DB_Text, Nullable: false},
			{Name: "过期时间", Type: DB_DateTime, Nullable: false},
			{Name: "创建", Type: DB_DateTime, Nullable: false},
			{Name: "更新", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"key"}, Type: UniqueIndex},
		},
	}

	// add v4
	mg.AddMigration("create dashboard_snapshot table v4", NewAddTableMigration(snapshotV4))
	mg.AddMigration("drop table dashboard_snapshot_v4 #1", NewDropTableMigration("dashboard_snapshot"))

	snapshotV5 := Table{
		Name: "dashboard_snapshot",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "名称", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "密钥", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "删除密钥", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "组织id", Type: DB_BigInt, Nullable: false},
			{Name: "用户id", Type: DB_BigInt, Nullable: false},
			{Name: "外部", Type: DB_Bool, Nullable: false},
			{Name: "外部url", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "仪表盘", Type: DB_Text, Nullable: false},
			{Name: "过期时间", Type: DB_DateTime, Nullable: false},
			{Name: "创建", Type: DB_DateTime, Nullable: false},
			{Name: "更新", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"key"}, Type: UniqueIndex},
			{Cols: []string{"delete_key"}, Type: UniqueIndex},
			{Cols: []string{"user_id"}},
		},
	}

	mg.AddMigration("create dashboard_snapshot table v5 #2", NewAddTableMigration(snapshotV5))
	addTableIndicesMigrations(mg, "v5", snapshotV5)

	// change column type of dashboard
	mg.AddMigration("alter dashboard_snapshot to mediumtext v2", NewRawSqlMigration("").
		Mysql("ALTER TABLE dashboard_snapshot MODIFY dashboard MEDIUMTEXT;"))

	mg.AddMigration("Update dashboard_snapshot table charset", NewTableCharsetMigration("dashboard_snapshot", []*Column{
		{Name: "名称", Type: DB_NVarchar, Length: 255, Nullable: false},
		{Name: "密钥", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "删除密钥", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "外部url", Type: DB_NVarchar, Length: 255, Nullable: false},
		{Name: "仪表盘", Type: DB_MediumText, Nullable: false},
	}))

	mg.AddMigration("Add column external_delete_url to dashboard_snapshots table", NewAddColumnMigration(snapshotV5, &Column{
		Name: "external_delete_url", Type: DB_NVarchar, Length: 255, Nullable: true,
	}))
}
