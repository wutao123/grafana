package migrations

import . "github.com/grafana/grafana/pkg/services/sqlstore/migrator"

func addTempUserMigrations(mg *Migrator) {
	tempUserV1 := Table{
		Name: "temp_user",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "组织id", Type: DB_BigInt, Nullable: false},
			{Name: "版本", Type: DB_Int, Nullable: false},
			{Name: "电子邮件", Type: DB_NVarchar, Length: 190},
			{Name: "名称", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "角色", Type: DB_NVarchar, Length: 20, Nullable: true},
			{Name: "代码", Type: DB_NVarchar, Length: 190},
			{Name: "状态", Type: DB_Varchar, Length: 20},
			{Name: "受用户id邀请", Type: DB_BigInt, Nullable: true},
			{Name: "已发送电子邮件", Type: DB_Bool},
			{Name: "电子邮件发送时间", Type: DB_DateTime, Nullable: true},
			{Name: "远程地址", Type: DB_Varchar, Length: 255, Nullable: true},
			{Name: "创建", Type: DB_DateTime},
			{Name: "更新", Type: DB_DateTime},
		},
		Indices: []*Index{
			{Cols: []string{"email"}, Type: IndexType},
			{Cols: []string{"org_id"}, Type: IndexType},
			{Cols: []string{"code"}, Type: IndexType},
			{Cols: []string{"status"}, Type: IndexType},
		},
	}

	// addDropAllIndicesMigrations(mg, "v7", tempUserV1)
	// mg.AddMigration("Drop old table tempUser v7", NewDropTableMigration("temp_user"))

	// create table
	mg.AddMigration("create temp user table v1-7", NewAddTableMigration(tempUserV1))
	addTableIndicesMigrations(mg, "v1-7", tempUserV1)

	mg.AddMigration("Update temp_user table charset", NewTableCharsetMigration("temp_user", []*Column{
		{Name: "电子邮件", Type: DB_NVarchar, Length: 190},
		{Name: "名称", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "角色", Type: DB_NVarchar, Length: 20, Nullable: true},
		{Name: "代码", Type: DB_NVarchar, Length: 190},
		{Name: "状态", Type: DB_Varchar, Length: 20},
		{Name: "远程地址", Type: DB_Varchar, Length: 255, Nullable: true},
	}))
}
