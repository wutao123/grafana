package migrations

import (
	. "github.com/grafana/grafana/pkg/services/sqlstore/migrator"
)

func addUserAuthTokenMigrations(mg *Migrator) {
	userAuthTokenV1 := Table{
		Name: "user_auth_token",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "用户id", Type: DB_BigInt, Nullable: false},
			{Name: "认证令牌", Type: DB_NVarchar, Length: 100, Nullable: false},
			{Name: "上一个身份验证令牌", Type: DB_NVarchar, Length: 100, Nullable: false},
			{Name: "用户代理", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "客户端ip", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "已看到身份验证令牌", Type: DB_Bool, Nullable: false},
			{Name: "看到了", Type: DB_Int, Nullable: true},
			{Name: "旋转位置", Type: DB_Int, Nullable: false},
			{Name: "创建于", Type: DB_Int, Nullable: false},
			{Name: "更新于", Type: DB_Int, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"auth_token"}, Type: UniqueIndex},
			{Cols: []string{"prev_auth_token"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("create user auth token table", NewAddTableMigration(userAuthTokenV1))
	mg.AddMigration("add unique index user_auth_token.auth_token", NewAddIndexMigration(userAuthTokenV1, userAuthTokenV1.Indices[0]))
	mg.AddMigration("add unique index user_auth_token.prev_auth_token", NewAddIndexMigration(userAuthTokenV1, userAuthTokenV1.Indices[1]))
}
