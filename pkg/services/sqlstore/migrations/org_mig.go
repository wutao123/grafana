package migrations

import . "github.com/grafana/grafana/pkg/services/sqlstore/migrator"

func addOrgMigrations(mg *Migrator) {
	orgV1 := Table{
		Name: "org",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "版本", Type: DB_Int, Nullable: false},
			{Name: "名称", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "地址1", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "地址2", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "城市", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "状态", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "邮政编码", Type: DB_NVarchar, Length: 50, Nullable: true},
			{Name: "国家", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "帐单电子邮件", Type: DB_NVarchar, Length: 255, Nullable: true},
			{Name: "创建", Type: DB_DateTime, Nullable: false},
			{Name: "更新", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"name"}, Type: UniqueIndex},
		},
	}

	// add org v1
	mg.AddMigration("create org table v1", NewAddTableMigration(orgV1))
	addTableIndicesMigrations(mg, "v1", orgV1)

	orgUserV1 := Table{
		Name: "org_user",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "组织id", Type: DB_BigInt},
			{Name: "用户id", Type: DB_BigInt},
			{Name: "角色", Type: DB_NVarchar, Length: 20},
			{Name: "创建", Type: DB_DateTime},
			{Name: "更新", Type: DB_DateTime},
		},
		Indices: []*Index{
			{Cols: []string{"org_id"}},
			{Cols: []string{"org_id", "user_id"}, Type: UniqueIndex},
		},
	}

	//-------  org_user table -------------------
	mg.AddMigration("create org_user table v1", NewAddTableMigration(orgUserV1))
	addTableIndicesMigrations(mg, "v1", orgUserV1)

	mg.AddMigration("Update org table charset", NewTableCharsetMigration("org", []*Column{
		{Name: "名称", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "地址1", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "地址2", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "城市", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "状态", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "邮政编码", Type: DB_NVarchar, Length: 50, Nullable: true},
		{Name: "国家", Type: DB_NVarchar, Length: 255, Nullable: true},
		{Name: "帐单电子邮件", Type: DB_NVarchar, Length: 255, Nullable: true},
	}))

	mg.AddMigration("Update org_user table charset", NewTableCharsetMigration("org_user", []*Column{
		{Name: "role", Type: DB_NVarchar, Length: 20},
	}))

	const migrateReadOnlyViewersToViewers = `UPDATE org_user SET role = 'Viewer' WHERE role = 'Read Only Editor'`
	mg.AddMigration("Migrate all Read Only Viewers to Viewers", NewRawSqlMigration(migrateReadOnlyViewersToViewers))
}
