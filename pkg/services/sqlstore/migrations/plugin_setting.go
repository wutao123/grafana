package migrations

import . "github.com/grafana/grafana/pkg/services/sqlstore/migrator"

func addAppSettingsMigration(mg *Migrator) {

	pluginSettingTable := Table{
		Name: "plugin_setting",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "组织id", Type: DB_BigInt, Nullable: true},
			{Name: "插件id", Type: DB_NVarchar, Length: 190, Nullable: false},
			{Name: "启用", Type: DB_Bool, Nullable: false},
			{Name: "固定的", Type: DB_Bool, Nullable: false},
			{Name: "json数据", Type: DB_Text, Nullable: true},
			{Name: "安全的json数据", Type: DB_Text, Nullable: true},
			{Name: "创建", Type: DB_DateTime, Nullable: false},
			{Name: "更新", Type: DB_DateTime, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"org_id", "plugin_id"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("create plugin_setting table", NewAddTableMigration(pluginSettingTable))

	//-------  indexes ------------------
	addTableIndicesMigrations(mg, "v1", pluginSettingTable)

	// add column to store installed version
	mg.AddMigration("Add column plugin_version to plugin_settings", NewAddColumnMigration(pluginSettingTable, &Column{
		Name: "plugin_version", Type: DB_NVarchar, Nullable: true, Length: 50,
	}))

	mg.AddMigration("Update plugin_setting table charset", NewTableCharsetMigration("plugin_setting", []*Column{
		{Name: "插件id", Type: DB_NVarchar, Length: 190, Nullable: false},
		{Name: "json数据", Type: DB_Text, Nullable: true},
		{Name: "安全的json数据", Type: DB_Text, Nullable: true},
		{Name: "插件版本", Type: DB_NVarchar, Nullable: true, Length: 50},
	}))
}
