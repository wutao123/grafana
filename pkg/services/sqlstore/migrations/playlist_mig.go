package migrations

import . "github.com/grafana/grafana/pkg/services/sqlstore/migrator"

func addPlaylistMigrations(mg *Migrator) {
	mg.AddMigration("Drop old table playlist table", NewDropTableMigration("playlist"))
	mg.AddMigration("Drop old table playlist_item table", NewDropTableMigration("playlist_item"))

	playlistV2 := Table{
		Name: "playlist",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "名称", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "间隔", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "组织id", Type: DB_BigInt, Nullable: false},
		},
	}

	// create table
	mg.AddMigration("create playlist table v2", NewAddTableMigration(playlistV2))

	playlistItemV2 := Table{
		Name: "playlist_item",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "播放列表id", Type: DB_BigInt, Nullable: false},
			{Name: "类型", Type: DB_NVarchar, Length: 255, Nullable: false},
			{Name: "值", Type: DB_Text, Nullable: false},
			{Name: "标题", Type: DB_Text, Nullable: false},
			{Name: "排序", Type: DB_Int, Nullable: false},
		},
	}

	mg.AddMigration("create playlist item table v2", NewAddTableMigration(playlistItemV2))

	mg.AddMigration("Update playlist table charset", NewTableCharsetMigration("playlist", []*Column{
		{Name: "名称", Type: DB_NVarchar, Length: 255, Nullable: false},
		{Name: "间隔", Type: DB_NVarchar, Length: 255, Nullable: false},
	}))

	mg.AddMigration("Update playlist_item table charset", NewTableCharsetMigration("playlist_item", []*Column{
		{Name: "类型", Type: DB_NVarchar, Length: 255, Nullable: false},
		{Name: "值", Type: DB_Text, Nullable: false},
		{Name: "标题", Type: DB_Text, Nullable: false},
	}))
}
