package migrations

import . "github.com/grafana/grafana/pkg/services/sqlstore/migrator"

func addDashboardVersionMigration(mg *Migrator) {
	dashboardVersionV1 := Table{
		Name: "dashboard_version",
		Columns: []*Column{
			{Name: "id", Type: DB_BigInt, IsPrimaryKey: true, IsAutoIncrement: true},
			{Name: "仪表盘id", Type: DB_BigInt},
			{Name: "父版本", Type: DB_Int, Nullable: false},
			{Name: "恢复自", Type: DB_Int, Nullable: false},
			{Name: "版本", Type: DB_Int, Nullable: false},
			{Name: "创建", Type: DB_DateTime, Nullable: false},
			{Name: "创建人", Type: DB_BigInt, Nullable: false},
			{Name: "消息", Type: DB_Text, Nullable: false},
			{Name: "数据", Type: DB_Text, Nullable: false},
		},
		Indices: []*Index{
			{Cols: []string{"dashboard_id"}},
			{Cols: []string{"dashboard_id", "version"}, Type: UniqueIndex},
		},
	}

	mg.AddMigration("create dashboard_version table v1", NewAddTableMigration(dashboardVersionV1))
	mg.AddMigration("add index dashboard_version.dashboard_id", NewAddIndexMigration(dashboardVersionV1, dashboardVersionV1.Indices[0]))
	mg.AddMigration("add unique index dashboard_version.dashboard_id and dashboard_version.version", NewAddIndexMigration(dashboardVersionV1, dashboardVersionV1.Indices[1]))

	// before new dashboards where created with version 0, now they are always inserted with version 1
	const setVersionTo1WhereZeroSQL = `UPDATE dashboard SET version = 1 WHERE version = 0`
	mg.AddMigration("Set dashboard version to 1 where 0", NewRawSqlMigration(setVersionTo1WhereZeroSQL))

	const rawSQL = `INSERT INTO dashboard_version
(
	dashboard_id,
	version,
	parent_version,
	restored_from,
	created,
	created_by,
	message,
	data
)
SELECT
	dashboard.id,
	dashboard.version,
	dashboard.version,
	dashboard.version,
	dashboard.updated,
	COALESCE(dashboard.updated_by, -1),
	'',
	dashboard.data
FROM dashboard;`
	mg.AddMigration("save existing dashboard data in dashboard_version table v1", NewRawSqlMigration(rawSQL))

	// change column type of dashboard_version.data
	mg.AddMigration("alter dashboard_version.data to mediumtext v1", NewRawSqlMigration("").
		Mysql("ALTER TABLE dashboard_version MODIFY data MEDIUMTEXT;"))
}
