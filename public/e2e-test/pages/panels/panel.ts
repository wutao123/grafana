import { ClickablePageObjectType, TestPage } from '@grafana/toolkit/src/e2e';

export interface Panel {
  panelTitle: ClickablePageObjectType;
  share: ClickablePageObjectType;
}

export const panel = new TestPage<Panel>({
  pageObjects: {
    panelTitle: '面板标题',
    share: '共享面板菜单项',
  },
});
