import { ClickablePageObjectType, TestPage } from '@grafana/toolkit/src/e2e';

export interface SharePanelModal {
  directLinkRenderedImage: ClickablePageObjectType;
}

export const sharePanelModal = new TestPage<SharePanelModal>({
  pageObjects: {
    directLinkRenderedImage: '链接到渲染图像',
  },
});
