import { ClickablePageObjectType, Selector, SelectPageObjectType, TestPage } from '@grafana/toolkit/src/e2e';

export interface EditPanelPage {
  queriesTab: ClickablePageObjectType;
  saveDashboard: ClickablePageObjectType;
  scenarioSelect: SelectPageObjectType;
  showXAxis: ClickablePageObjectType;
  visualizationTab: ClickablePageObjectType;
}

export const editPanelPage = new TestPage<EditPanelPage>({
  pageObjects: {
    queriesTab: '“查询”选项卡按钮',
    saveDashboard: '保存仪表板导航栏按钮',
    scenarioSelect: '方案选择',
    showXAxis: () => Selector.fromSelector('[aria-label="X-Axis section"] [label=Show] .gf-form-switch'),
    visualizationTab: '可视化选项卡按钮',
  },
});
