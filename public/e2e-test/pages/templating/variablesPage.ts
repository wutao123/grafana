import { ArrayPageObjectType, ClickablePageObjectType, TestPage } from '@grafana/toolkit/src/e2e';

export interface VariablesPage {
  callToActionButton: ClickablePageObjectType;
  variableTableNameField: ArrayPageObjectType;
  variableTableDefinitionField: ArrayPageObjectType;
  variableTableArrowUpButton: ArrayPageObjectType;
  variableTableArrowDownButton: ArrayPageObjectType;
  variableTableDuplicateButton: ArrayPageObjectType;
  variableTableRemoveButton: ArrayPageObjectType;
  newVariableButton: ClickablePageObjectType;
  goBackButton: ClickablePageObjectType;
}

export const variablesPage = new TestPage<VariablesPage>({
  pageObjects: {
    callToActionButton: '调用操作按钮添加变量',
    variableTableNameField: '变量编辑器表名字段',
    variableTableDefinitionField: '变量编辑器表定义字段',
    variableTableArrowUpButton: '变量编辑器表格ArrowUp按钮',
    variableTableArrowDownButton: '变量编辑器表格ArrowDown按钮',
    variableTableDuplicateButton: '变量编辑器表复制按钮',
    variableTableRemoveButton: '变量编辑器表删除按钮',
    newVariableButton: '变量编辑器“新建变量”按钮',
    goBackButton: '仪表板设置返回按钮',
  },
});

export interface AssertVariableTableArguments {
  name: string;
  query: string;
}

export const assertVariableTable = async (page: TestPage<VariablesPage>, args: AssertVariableTableArguments[]) => {
  console.log('Asserting variable table');
  await page.pageObjects.variableTableNameField.waitForSelector();
  await page.pageObjects.variableTableNameField.hasLength(args.length);
  await page.pageObjects.variableTableDefinitionField.hasLength(args.length);
  await page.pageObjects.variableTableArrowUpButton.hasLength(args.length);
  await page.pageObjects.variableTableArrowDownButton.hasLength(args.length);
  await page.pageObjects.variableTableDuplicateButton.hasLength(args.length);
  await page.pageObjects.variableTableRemoveButton.hasLength(args.length);
  for (let index = 0; index < args.length; index++) {
    const { name, query } = args[index];
    await page.pageObjects.variableTableNameField.containsTextAtPos(`$${name}`, index);
    await page.pageObjects.variableTableDefinitionField.containsTextAtPos(query, index);
  }
  console.log('Asserting variable table, Ok');
};
