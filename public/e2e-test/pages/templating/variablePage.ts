import {
  ClickablePageObjectType,
  InputPageObjectType,
  PageObjectType,
  Selector,
  SelectPageObjectType,
  SwitchPageObjectType,
  TestPage,
} from '@grafana/toolkit/src/e2e';

export interface VariablePage {
  headerLink: ClickablePageObjectType;
  modeLabel: PageObjectType;
  generalNameInput: InputPageObjectType;
  generalTypeSelect: SelectPageObjectType;
  generalLabelInput: InputPageObjectType;
  generalHideSelect: SelectPageObjectType;
  queryOptionsDataSourceSelect: SelectPageObjectType;
  queryOptionsRefreshSelect: SelectPageObjectType;
  queryOptionsRegExInput: InputPageObjectType;
  queryOptionsSortSelect: SelectPageObjectType;
  queryOptionsQueryInput: InputPageObjectType;
  selectionOptionsMultiSwitch: SwitchPageObjectType;
  selectionOptionsIncludeAllSwitch: SwitchPageObjectType;
  selectionOptionsCustomAllInput: InputPageObjectType;
  valueGroupsTagsEnabledSwitch: SwitchPageObjectType;
  valueGroupsTagsTagsQueryInput: InputPageObjectType;
  valueGroupsTagsTagsValuesQueryInput: InputPageObjectType;
  previewOfValuesOption: PageObjectType;
  addButton: ClickablePageObjectType;
  updateButton: ClickablePageObjectType;
}

export const variablePage = new TestPage<VariablePage>({
  pageObjects: {
    headerLink: '变量编辑器标题链接',
    modeLabel: '变量编辑器标题模式新建',
    generalNameInput: '变量编辑器窗体名称字段',
    generalTypeSelect: '变量编辑器窗体类型选择',
    generalLabelInput: '变量编辑器窗体标签字段',
    generalHideSelect: '变量编辑器窗体隐藏选择',
    queryOptionsDataSourceSelect: '变量编辑器窗体查询数据源选择',
    queryOptionsRefreshSelect: '变量编辑器窗体查询刷新选择',
    queryOptionsRegExInput: '变量编辑器窗体查询正则表达式字段',
    queryOptionsSortSelect: '变量编辑器窗体查询排序选择',
    queryOptionsQueryInput: '变量编辑器窗体默认变量查询编辑器文本区域',
    selectionOptionsMultiSwitch: () => Selector.fromSwitchLabel('变量编辑器窗体多开关'),
    selectionOptionsIncludeAllSwitch: () => Selector.fromSwitchLabel('变量编辑器窗体IncludeAll开关'),
    selectionOptionsCustomAllInput: '变量编辑器窗体IncludeAll字段',
    valueGroupsTagsEnabledSwitch: () => Selector.fromSwitchLabel('变量编辑器窗体查询使用标记开关'),
    valueGroupsTagsTagsQueryInput: '变量编辑器窗体查询TagsQuery字段',
    valueGroupsTagsTagsValuesQueryInput: '变量编辑器窗体查询TagsValuesQuery字段',
    previewOfValuesOption: '变量编辑器值预览选项',
    addButton: '变量编辑器添加按钮',
    updateButton: '变量编辑器更新按钮',
  },
});

export interface CreateQueryVariableArguments {
  page: TestPage<VariablePage>;
  name: string;
  label: string;
  datasourceName: string;
  query: string;
}

export const createQueryVariable = async ({
  page,
  name,
  label,
  datasourceName,
  query,
}: CreateQueryVariableArguments) => {
  console.log('Creating a Query Variable with required');
  await page.pageObjects.generalNameInput.enter(name);
  await page.pageObjects.generalLabelInput.enter(label);
  await page.pageObjects.queryOptionsDataSourceSelect.select(`string:${datasourceName}`);
  await page.pageObjects.queryOptionsQueryInput.exists();
  await page.pageObjects.queryOptionsQueryInput.containsPlaceholder('metric name or tags query');
  await page.pageObjects.queryOptionsQueryInput.enter(query);
  await page.pageObjects.queryOptionsQueryInput.blur();
  await page.pageObjects.previewOfValuesOption.exists();
  await page.pageObjects.selectionOptionsMultiSwitch.toggle();
  await page.pageObjects.selectionOptionsMultiSwitch.isSwitchedOn();
  await page.pageObjects.selectionOptionsIncludeAllSwitch.toggle();
  await page.pageObjects.selectionOptionsIncludeAllSwitch.isSwitchedOn();
  await page.pageObjects.selectionOptionsCustomAllInput.exists();
  await page.pageObjects.selectionOptionsCustomAllInput.containsText('');
  await page.pageObjects.selectionOptionsCustomAllInput.containsPlaceholder('blank = auto');
  await page.pageObjects.addButton.click();
  console.log('Creating a Query Variable with required, OK!');
};
