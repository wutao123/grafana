import { Grammar } from 'prismjs';
import { CompletionItem } from '@grafana/ui';

const AGGREGATION_OPERATORS: CompletionItem[] = [
  {
    label: '总和',
    insertText: '总和',
    documentation: '计算维度上的和',
  },
  {
    label: '最小值',
    insertText: '最小值',
    documentation: '选择维度上的最小值',
  },
  {
    label: '最大值',
    insertText: '最大值',
    documentation: '选择维度上的最大值',
  },
  {
    label: '平均值',
    insertText: '平均值',
    documentation: '计算维度上的平均值',
  },
  {
    label: '标准偏差',
    insertText: '标准偏差',
    documentation: '计算维度上的总体标准偏差',
  },
  {
    label: '标准方差',
    insertText: '标准方差',
    documentation: '计算维度上的总体标准方差',
  },
  {
    label: '计数',
    insertText: '计数',
    documentation: '计算矢量中的元素数',
  },
  {
    label: '最小k',
    insertText: '最小k',
    documentation: '按样本值的最小k元素',
  },
  {
    label: '最大k',
    insertText: '最大k',
    documentation: '按样本值计算的最大k元素',
  },
];

export const RANGE_VEC_FUNCTIONS = [
  {
    insertText: '计数时间',
    label: '计数时间',
    detail: 'count_over_time(range-vector)',
    documentation: '指定间隔内所有值的计数。',
  },
  {
    insertText: '速率',
    label: '速率',
    detail: 'rate(v range-vector)',
    documentation:
      '计算范围向量中时间序列的每秒平均增长率。单调性中的中断（例如由于目标重新启动而导致的计数器重置）将自动调整。此外，计算会外推到时间范围的末端，从而允许遗漏的刮削或刮削周期与范围的时间周期不完全对齐。',
  },
];

export const FUNCTIONS = [...AGGREGATION_OPERATORS, ...RANGE_VEC_FUNCTIONS];

const tokenizer: Grammar = {
  comment: {
    pattern: /#.*/,
  },
  'context-aggregation': {
    pattern: /((without|by)\s*)\([^)]*\)/, // by ()
    lookbehind: true,
    inside: {
      'label-key': {
        pattern: /[^(),\s][^,)]*[^),\s]*/,
        alias: 'attr-name',
      },
      punctuation: /[()]/,
    },
  },
  'context-labels': {
    pattern: /\{[^}]*(?=})/,
    greedy: true,
    inside: {
      comment: {
        pattern: /#.*/,
      },
      'label-key': {
        pattern: /[a-z_]\w*(?=\s*(=|!=|=~|!~))/,
        alias: 'attr-name',
        greedy: true,
      },
      'label-value': {
        pattern: /"(?:\\.|[^\\"])*"/,
        greedy: true,
        alias: 'attr-value',
      },
      punctuation: /[{]/,
    },
  },
  function: new RegExp(`\\b(?:${FUNCTIONS.map(f => f.label).join('|')})(?=\\s*\\()`, 'i'),
  'context-range': [
    {
      pattern: /\[[^\]]*(?=\])/, // [1m]
      inside: {
        'range-duration': {
          pattern: /\b\d+[smhdwy]\b/i,
          alias: 'number',
        },
      },
    },
    {
      pattern: /(offset\s+)\w+/, // offset 1m
      lookbehind: true,
      inside: {
        'range-duration': {
          pattern: /\b\d+[smhdwy]\b/i,
          alias: 'number',
        },
      },
    },
  ],
  number: /\b-?\d+((\.\d*)?([eE][+-]?\d+)?)?\b/,
  operator: new RegExp(`/&&?|\\|?\\||!=?|<(?:=>?|<|>)?|>[>=]?`, 'i'),
  punctuation: /[{}()`,.]/,
};

export default tokenizer;
