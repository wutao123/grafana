import React, { PureComponent } from 'react';
import { shuffle } from 'lodash';
import { ExploreStartPageProps, DataQuery, ExploreMode } from '@grafana/data';
import LokiLanguageProvider from '../language_provider';

const DEFAULT_EXAMPLES = ['{job="default/prometheus"}'];
const PREFERRED_LABELS = ['job', 'app', 'k8s_app'];
const EXAMPLES_LIMIT = 5;

const LOGQL_EXAMPLES = [
  {
    title: '随时间计数',
    expression: 'count_over_time({job="mysql"}[5m])',
    label: '此查询统计MySQL作业在过去五分钟内的所有日志行。',
  },
  {
    title: '速率',
    expression: 'rate(({job="mysql"} |= "error" != "timeout")[10s])',
    label: '此查询获取MySQL作业在过去10秒内所有非超时错误的每秒速率。',
  },
  {
    title: '聚合、计数和分组',
    expression: 'sum(count_over_time({job="mysql"}[5m])) by (level)',
    label: '获取过去五分钟内按级别分组的日志计数。',
  },
];

export default class LokiCheatSheet extends PureComponent<ExploreStartPageProps, { userExamples: string[] }> {
  userLabelTimer: NodeJS.Timeout;
  state = {
    userExamples: DEFAULT_EXAMPLES,
  };

  componentDidMount() {
    this.scheduleUserLabelChecking();
  }

  componentWillUnmount() {
    clearTimeout(this.userLabelTimer);
  }

  scheduleUserLabelChecking() {
    this.userLabelTimer = setTimeout(this.checkUserLabels, 1000);
  }

  checkUserLabels = async () => {
    // Set example from user labels
    const provider: LokiLanguageProvider = this.props.datasource.languageProvider;
    if (provider.started) {
      const labels = provider.getLabelKeys() || [];
      const preferredLabel = PREFERRED_LABELS.find(l => labels.includes(l));
      if (preferredLabel) {
        const values = await provider.getLabelValues(preferredLabel);
        const userExamples = shuffle(values)
          .slice(0, EXAMPLES_LIMIT)
          .map(value => `{${preferredLabel}="${value}"}`);
        this.setState({ userExamples });
      }
    } else {
      this.scheduleUserLabelChecking();
    }
  };

  renderExpression(expr: string) {
    const { onClickExample } = this.props;

    return (
      <div
        className="cheat-sheet-item__example"
        key={expr}
        onClick={e => onClickExample({ refId: 'A', expr } as DataQuery)}
      >
        <code>{expr}</code>
      </div>
    );
  }

  renderLogsCheatSheet() {
    const { userExamples } = this.state;

    return (
      <>
        <h2>Loki备忘单</h2>
        <div className="cheat-sheet-item">
          <div className="cheat-sheet-item__title">查看日志</div>
          <div className="cheat-sheet-item__label">首先从日志标签选择器中选择一个日志流。</div>
          <div className="cheat-sheet-item__label">或者，可以将流选择器写入查询字段:</div>
          {this.renderExpression('{job="default/prometheus"}')}
          {userExamples !== DEFAULT_EXAMPLES && userExamples.length > 0 ? (
            <div>
              <div className="cheat-sheet-item__label">下面是一些来自日志的示例流:</div>
              {userExamples.map(example => this.renderExpression(example))}
            </div>
          ) : null}
        </div>
        <div className="cheat-sheet-item">
          <div className="cheat-sheet-item__title">组合流选择器</div>
          {this.renderExpression('{app="cassandra",namespace="prod"}')}
          <div className="cheat-sheet-item__label">返回具有两个标签的流中的所有日志行。</div>
        </div>

        <div className="cheat-sheet-item">
          <div className="cheat-sheet-item__title">筛选搜索词。</div>
          {this.renderExpression('{app="cassandra"} |~ "(duration|latency)s*(=|is|of)s*[d.]+"')}
          {this.renderExpression('{app="cassandra"} |= "exact match"')}
          {this.renderExpression('{app="cassandra"} != "do not match"')}
          <div className="cheat-sheet-item__label">
            <a href="https://github.com/grafana/loki/blob/master/docs/logql.md#filter-expression" target="logql">
              LogQL
            </a>{' '}
            支持精确表达式筛选器和正则表达式筛选器。
          </div>
        </div>
      </>
    );
  }

  renderMetricsCheatSheet() {
    return (
      <div>
        <h2>LogQL备忘单</h2>
        {LOGQL_EXAMPLES.map(item => (
          <div className="cheat-sheet-item" key={item.expression}>
            <div className="cheat-sheet-item__title">{item.title}</div>
            {this.renderExpression(item.expression)}
            <div className="cheat-sheet-item__label">{item.label}</div>
          </div>
        ))}
      </div>
    );
  }

  render() {
    const { exploreMode } = this.props;

    return exploreMode === ExploreMode.Logs ? this.renderLogsCheatSheet() : this.renderMetricsCheatSheet();
  }
}
