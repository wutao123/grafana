import React from 'react';
import { FormField } from '@grafana/ui';

type Props = {
  value: string;
  onChange: (value: string) => void;
};

export const MaxLinesField = (props: Props) => {
  const { value, onChange } = props;
  return (
    <FormField
      label="最大行数"
      labelWidth={11}
      inputWidth={20}
      inputEl={
        <input
          type="number"
          className="gf-form-input width-8 gf-form-input--has-help-icon"
          value={value}
          onChange={event => onChange(event.currentTarget.value)}
          spellCheck={false}
          placeholder="1000"
        />
      }
      tooltip={
        <>
          Loki查询必须包含返回的最大行数限制（默认值：1000）。增加这个限制，以便有一个更大的结果集用于特别分析。如果您的浏览器在显示日志结果时变得迟缓，请降低此限制。
        </>
      }
    />
  );
};
