import React from 'react';
import { ExploreStartPageProps, DataQuery } from '@grafana/data';

const CHEAT_SHEET_ITEMS = [
  {
    title: '请求率',
    expression: 'rate(http_request_total[5m])',
    label: '给定一个HTTP请求计数器，此查询计算过去5分钟内每秒的平均请求速率。',
  },
  {
    title: '请求延迟的95%',
    expression: 'histogram_quantile(0.95, sum(rate(prometheus_http_request_duration_seconds_bucket[5m])) by (le))',
    label: '计算超过5分钟窗口的HTTP请求率的95%。',
  },
  {
    title: '告警触发',
    expression: 'sort_desc(sum(sum_over_time(ALERTS{alertstate="firing"}[24h])) by (alertname))',
    label: '总结过去24小时内发出的警报。',
  },
  {
    title: '步骤',
    label:
      '使用持续时间格式定义图形分辨率 (15s, 1m, 3h, ...)。小步骤创建高分辨率图形，但在较大的时间范围内可能会很慢。使用较长的步骤会降低分辨率，并通过生成较少的数据点来平滑图形。如果没有给出步骤，则会自动计算分辨率。',
  },
];

export default (props: ExploreStartPageProps) => (
  <div>
    <h2>PromQL备忘单</h2>
    {CHEAT_SHEET_ITEMS.map((item, index) => (
      <div className="cheat-sheet-item" key={index}>
        <div className="cheat-sheet-item__title">{item.title}</div>
        {item.expression ? (
          <div
            className="cheat-sheet-item__example"
            onClick={e => props.onClickExample({ refId: 'A', expr: item.expression } as DataQuery)}
          >
            <code>{item.expression}</code>
          </div>
        ) : null}
        <div className="cheat-sheet-item__label">{item.label}</div>
      </div>
    ))}
  </div>
);
