import { CompletionItem } from '@grafana/ui';

export const RATE_RANGES: CompletionItem[] = [
  { label: '$__interval', sortText: '$__interval' },
  { label: '1m', sortText: '00:01:00' },
  { label: '5m', sortText: '00:05:00' },
  { label: '10m', sortText: '00:10:00' },
  { label: '30m', sortText: '00:30:00' },
  { label: '1h', sortText: '01:00:00' },
  { label: '1d', sortText: '24:00:00' },
];

export const OPERATORS = ['by', 'group_left', 'group_right', 'ignoring', 'on', 'offset', 'without'];

const AGGREGATION_OPERATORS: CompletionItem[] = [
  {
    label: '总和',
    insertText: '总和',
    documentation: '计算维度上的和',
  },
  {
    label: '最小值',
    insertText: '最小值',
    documentation: '选择维度上的最小值',
  },
  {
    label: '最大值',
    insertText: '最大值',
    documentation: '选择维度上的最大值',
  },
  {
    label: '平均值',
    insertText: '平均值',
    documentation: '计算维度上的平均值',
  },
  {
    label: '标准偏差',
    insertText: '标准偏差',
    documentation: '计算维度上的总体标准偏差',
  },
  {
    label: '标准方差',
    insertText: '标准方差',
    documentation: '计算维度上的总体标准方差',
  },
  {
    label: '计数',
    insertText: '计数',
    documentation: '计算矢量中的元素数',
  },
  {
    label: '计数值',
    insertText: '计数值',
    documentation: '计数具有相同值的元素数',
  },
  {
    label: '最小k个',
    insertText: '最小k个',
    documentation: '按样本值的最小k元素',
  },
  {
    label: '最大k个',
    insertText: '最大k个',
    documentation: '按样本值计算的最大k元素',
  },
  {
    label: '分位数',
    insertText: '分位数',
    documentation: '计算维度上的φ-分位数 (0 ≤ φ ≤ 1) ',
  },
];

export const FUNCTIONS = [
  ...AGGREGATION_OPERATORS,
  {
    insertText: '绝对值',
    label: '绝对值',
    detail: 'abs(v instant-vector)',
    documentation: '返回将所有采样值转换为其绝对值的输入向量。',
  },
  {
    insertText: '不存在',
    label: '不存在',
    detail: 'absent(v instant-vector)',
    documentation:
      '如果传递给它的向量有任何元素，则返回空向量；如果传递给它的向量没有元素，则返回值为1的1-元素向量。当给定度量名称和标签组合不存在时间序列时，这对于发出警报非常有用。',
  },
  {
    insertText: 'ceil',
    label: 'ceil',
    detail: 'ceil(v instant-vector)',
    documentation: '将“v”中所有元素的采样值舍入到最接近的整数。',
  },
  {
    insertText: '改变',
    label: '改变',
    detail: 'changes(v range-vector)',
    documentation:
      '对于每个输入时间序列，`changes（v range vector）`将其值在所提供的时间范围内更改的次数作为即时向量返回。',
  },
  {
    insertText: '钳制为最大值',
    label: '钳制为最大值',
    detail: 'clamp_max(v instant-vector, max scalar)',
    documentation: '将“v”中所有元素的采样值钳制为“最大值”。',
  },
  {
    insertText: '钳制为最小值',
    label: '钳制为最小值',
    detail: 'clamp_min(v instant-vector, min scalar)',
    documentation: '将“v”中所有元素的采样值钳制为“最小值”。',
  },
  {
    insertText: '标量计数',
    label: '标量计数',
    detail: 'count_scalar(v instant-vector)',
    documentation:
      '以标量形式返回时间序列向量中的元素数。这与“count()”聚合运算符不同，后者总是返回一个向量（如果输入向量为空，则为空），并允许通过“by”子句按标签分组。',
  },
  {
    insertText: '月份日期几号',
    label: '月份日期几号',
    detail: 'day_of_month(v=vector(time()) instant-vector)',
    documentation: '返回以UTC表示的每个给定时间的月份日期。返回值从1到31。',
  },
  {
    insertText: '星期几',
    label: '星期几',
    detail: 'day_of_week(v=vector(time()) instant-vector)',
    documentation: '返回以UTC表示的每个给定时间的星期几。返回的值从0到6，其中0表示星期日等。',
  },
  {
    insertText: '每月天数',
    label: '每月天数',
    detail: 'days_in_month(v=vector(time()) instant-vector)',
    documentation: '返回以UTC表示的给定时间的每月天数。返回值从28到31。',
  },
  {
    insertText: 'delta',
    label: 'delta',
    detail: 'delta(v range-vector)',
    documentation:
      '计算范围向量“v”中每个时间序列元素的第一个值和最后一个值之间的差，返回具有给定增量和等效标签的即时向量。delta被外推以覆盖范围向量选择器中指定的整个时间范围，因此即使样本值都是整数，也可能得到非整数结果。',
  },
  {
    insertText: '导数',
    label: '导数',
    detail: 'deriv(v range-vector)',
    documentation: '使用简单的线性回归计算范围向量“v”中时间序列的每秒导数。',
  },
  {
    insertText: '删除相同标签',
    label: '删除相同标签',
    detail: 'drop_common_labels(instant-vector)',
    documentation: '删除输入向量中所有序列中具有相同名称和值的所有标签。',
  },
  {
    insertText: '指数',
    label: '指数',
    detail: 'exp(v instant-vector)',
    documentation: '计算“v”中所有元素的指数函数。\n特殊情况包括：\n* `Exp(+Inf) = +Inf` \n* `Exp(NaN) = NaN`',
  },
  {
    insertText: '向下取整',
    label: '向下取整',
    detail: 'floor(v instant-vector)',
    documentation: '将“v”中所有元素的采样值舍入到最接近的整数。',
  },
  {
    insertText: '直方图分位数',
    label: '直方图分位数',
    detail: 'histogram_quantile(φ float, b instant-vector)',
    documentation:
      '从直方图的`b`桶计算φ-分位数（0≤φ≤1）。“b”中的样本是每个bucket中的观测计数。每个样本都必须有一个标签“le”，其中标签值表示bucket的包含上限。（没有这样标签的样本将被忽略。）直方图度量类型自动提供带有“_bucket”后缀和适当标签的时间序列。',
  },
  {
    insertText: 'holt_winters',
    label: 'holt_winters',
    detail: 'holt_winters(v range-vector, sf scalar, tf scalar)',
    documentation:
      '根据“v”中的范围为时间序列生成平滑值。平滑因子“sf”越低，对旧数据的重要性就越大。趋势因子“tf”越高，数据中考虑的趋势就越多。“sf”和“tf”必须介于0和1之间。',
  },
  {
    insertText: '小时数',
    label: '小时数',
    detail: 'hour(v=vector(time()) instant-vector)',
    documentation: '返回UTC中给定时间的每一天的小时数。返回的值从0到23。',
  },
  {
    insertText: 'idelta',
    label: 'idelta',
    detail: 'idelta(v range-vector)',
    documentation: '计算范围向量“v”中最后两个样本之间的差异，返回具有给定增量和等效标签的即时向量。',
  },
  {
    insertText: 'increase',
    label: 'increase',
    detail: 'increase(v range-vector)',
    documentation:
      '计算范围向量中时间序列的增量。单调性中的中断（例如由于目标重新启动而导致的计数器重置）将自动调整。增量被外推以覆盖范围向量选择器中指定的整个时间范围，因此即使计数器仅以整数增量增加，也可能获得非整数结果。',
  },
  {
    insertText: 'irate',
    label: 'irate',
    detail: 'irate(v range-vector)',
    documentation:
      'Calculates the per-second instant rate of increase of the time series in the range vector. This is based on the last two data points. Breaks in monotonicity (such as counter resets due to target restarts) are automatically adjusted for.',
  },
  {
    insertText: 'label_replace',
    label: 'label_replace',
    detail: 'label_replace(v instant-vector, dst_label string, replacement string, src_label string, regex string)',
    documentation:
      "For each timeseries in `v`, `label_replace(v instant-vector, dst_label string, replacement string, src_label string, regex string)`  matches the regular expression `regex` against the label `src_label`.  If it matches, then the timeseries is returned with the label `dst_label` replaced by the expansion of `replacement`. `$1` is replaced with the first matching subgroup, `$2` with the second etc. If the regular expression doesn't match then the timeseries is returned unchanged.",
  },
  {
    insertText: 'ln',
    label: 'ln',
    detail: 'ln(v instant-vector)',
    documentation:
      'calculates the natural logarithm for all elements in `v`.\nSpecial cases are:\n * `ln(+Inf) = +Inf`\n * `ln(0) = -Inf`\n * `ln(x < 0) = NaN`\n * `ln(NaN) = NaN`',
  },
  {
    insertText: 'log2',
    label: 'log2',
    detail: 'log2(v instant-vector)',
    documentation:
      'Calculates the binary logarithm for all elements in `v`. The special cases are equivalent to those in `ln`.',
  },
  {
    insertText: 'log10',
    label: 'log10',
    detail: 'log10(v instant-vector)',
    documentation:
      'Calculates the decimal logarithm for all elements in `v`. The special cases are equivalent to those in `ln`.',
  },
  {
    insertText: 'minute',
    label: 'minute',
    detail: 'minute(v=vector(time()) instant-vector)',
    documentation:
      'Returns the minute of the hour for each of the given times in UTC. Returned values are from 0 to 59.',
  },
  {
    insertText: 'month',
    label: 'month',
    detail: 'month(v=vector(time()) instant-vector)',
    documentation:
      'Returns the month of the year for each of the given times in UTC. Returned values are from 1 to 12, where 1 means January etc.',
  },
  {
    insertText: 'predict_linear',
    label: 'predict_linear',
    detail: 'predict_linear(v range-vector, t scalar)',
    documentation:
      'Predicts the value of time series `t` seconds from now, based on the range vector `v`, using simple linear regression.',
  },
  {
    insertText: 'rate',
    label: 'rate',
    detail: 'rate(v range-vector)',
    documentation:
      "Calculates the per-second average rate of increase of the time series in the range vector. Breaks in monotonicity (such as counter resets due to target restarts) are automatically adjusted for. Also, the calculation extrapolates to the ends of the time range, allowing for missed scrapes or imperfect alignment of scrape cycles with the range's time period.",
  },
  {
    insertText: 'resets',
    label: 'resets',
    detail: 'resets(v range-vector)',
    documentation:
      'For each input time series, `resets(v range-vector)` returns the number of counter resets within the provided time range as an instant vector. Any decrease in the value between two consecutive samples is interpreted as a counter reset.',
  },
  {
    insertText: 'round',
    label: 'round',
    detail: 'round(v instant-vector, to_nearest=1 scalar)',
    documentation:
      'Rounds the sample values of all elements in `v` to the nearest integer. Ties are resolved by rounding up. The optional `to_nearest` argument allows specifying the nearest multiple to which the sample values should be rounded. This multiple may also be a fraction.',
  },
  {
    insertText: 'scalar',
    label: 'scalar',
    detail: 'scalar(v instant-vector)',
    documentation:
      'Given a single-element input vector, `scalar(v instant-vector)` returns the sample value of that single element as a scalar. If the input vector does not have exactly one element, `scalar` will return `NaN`.',
  },
  {
    insertText: 'sort',
    label: 'sort',
    detail: 'sort(v instant-vector)',
    documentation: 'Returns vector elements sorted by their sample values, in ascending order.',
  },
  {
    insertText: 'sort_desc',
    label: 'sort_desc',
    detail: 'sort_desc(v instant-vector)',
    documentation: 'Returns vector elements sorted by their sample values, in descending order.',
  },
  {
    insertText: 'sqrt',
    label: 'sqrt',
    detail: 'sqrt(v instant-vector)',
    documentation: 'Calculates the square root of all elements in `v`.',
  },
  {
    insertText: 'time',
    label: 'time',
    detail: 'time()',
    documentation:
      'Returns the number of seconds since January 1, 1970 UTC. Note that this does not actually return the current time, but the time at which the expression is to be evaluated.',
  },
  {
    insertText: 'vector',
    label: 'vector',
    detail: 'vector(s scalar)',
    documentation: 'Returns the scalar `s` as a vector with no labels.',
  },
  {
    insertText: 'year',
    label: 'year',
    detail: 'year(v=vector(time()) instant-vector)',
    documentation: 'Returns the year for each of the given times in UTC.',
  },
  {
    insertText: 'avg_over_time',
    label: 'avg_over_time',
    detail: 'avg_over_time(range-vector)',
    documentation: 'The average value of all points in the specified interval.',
  },
  {
    insertText: 'min_over_time',
    label: 'min_over_time',
    detail: 'min_over_time(range-vector)',
    documentation: 'The minimum value of all points in the specified interval.',
  },
  {
    insertText: 'max_over_time',
    label: 'max_over_time',
    detail: 'max_over_time(range-vector)',
    documentation: 'The maximum value of all points in the specified interval.',
  },
  {
    insertText: 'sum_over_time',
    label: 'sum_over_time',
    detail: 'sum_over_time(range-vector)',
    documentation: 'The sum of all values in the specified interval.',
  },
  {
    insertText: 'count_over_time',
    label: 'count_over_time',
    detail: 'count_over_time(range-vector)',
    documentation: 'The count of all values in the specified interval.',
  },
  {
    insertText: 'quantile_over_time',
    label: '随时间变化的分位数',
    detail: 'quantile_over_time(scalar, range-vector)',
    documentation: '在指定区间内的值的φ-分位数(0 ≤ φ ≤ 1)。',
  },
  {
    insertText: 'stddev_over_time',
    label: 'stddev_over_time',
    detail: 'stddev_over_time(range-vector)',
    documentation: '指定间隔内值的总体标准偏差。',
  },
  {
    insertText: 'stdvar_over_time',
    label: '随时间推移的标准方差',
    detail: 'stdvar_over_time(range-vector)',
    documentation: '指定间隔内值的总体标准方差。',
  },
];

const tokenizer = {
  comment: {
    pattern: /#.*/,
  },
  'context-aggregation': {
    pattern: /((by|without)\s*)\([^)]*\)/, // by ()
    lookbehind: true,
    inside: {
      'label-key': {
        pattern: /[^(),\s][^,)]*[^),\s]*/,
        alias: 'attr-name',
      },
      punctuation: /[()]/,
    },
  },
  'context-labels': {
    pattern: /\{[^}]*(?=})/,
    greedy: true,
    inside: {
      comment: {
        pattern: /#.*/,
      },
      'label-key': {
        pattern: /[a-z_]\w*(?=\s*(=|!=|=~|!~))/,
        alias: 'attr-name',
        greedy: true,
      },
      'label-value': {
        pattern: /"(?:\\.|[^\\"])*"/,
        greedy: true,
        alias: 'attr-value',
      },
      punctuation: /[{]/,
    },
  },
  function: new RegExp(`\\b(?:${FUNCTIONS.map(f => f.label).join('|')})(?=\\s*\\()`, 'i'),
  'context-range': [
    {
      pattern: /\[[^\]]*(?=])/, // [1m]
      inside: {
        'range-duration': {
          pattern: /\b\d+[smhdwy]\b/i,
          alias: 'number',
        },
      },
    },
    {
      pattern: /(offset\s+)\w+/, // offset 1m
      lookbehind: true,
      inside: {
        'range-duration': {
          pattern: /\b\d+[smhdwy]\b/i,
          alias: 'number',
        },
      },
    },
  ],
  number: /\b-?\d+((\.\d*)?([eE][+-]?\d+)?)?\b/,
  operator: new RegExp(`/[-+*/=%^~]|&&?|\\|?\\||!=?|<(?:=>?|<|>)?|>[>=]?|\\b(?:${OPERATORS.join('|')})\\b`, 'i'),
  punctuation: /[{};()`,.]/,
};

export default tokenizer;
