export const metricDescriptors = [
  {
    name: 'projects/grafana-prod/metricDescriptors/agent.googleapis.com/agent/api_request_count',
    labels: [
      {
        key: 'state',
        description: '请求状态',
      },
    ],
    metricKind: 'CUMULATIVE',
    valueType: 'INT64',
    unit: '1',
    description: 'API请求计数',
    displayName: 'API请求计数',
    type: 'agent.googleapis.com/agent/api_request_count',
    metadata: {
      launchStage: 'GA',
      samplePeriod: '60s',
      ingestDelay: '0s',
    },
  },
  {
    name: 'projects/grafana-prod/metricDescriptors/agent.googleapis.com/agent/log_entry_count',
    labels: [
      {
        key: 'response_code',
        description: 'HTTP响应代码',
      },
    ],
    metricKind: 'CUMULATIVE',
    valueType: 'INT64',
    unit: '1',
    description: '日志项写入计数',
    displayName: '日志项计数',
    type: 'agent.googleapis.com/agent/log_entry_count',
    metadata: {
      launchStage: 'GA',
      samplePeriod: '60s',
      ingestDelay: '0s',
    },
  },
];
