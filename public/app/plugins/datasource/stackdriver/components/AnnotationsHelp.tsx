import React, { FC } from 'react';

export const AnnotationsHelp: FC = () => {
  return (
    <div className="gf-form grafana-info-box alert-info">
      <div>
        <h5>注释查询格式</h5>
        <p>注释是覆盖在图形顶部的事件。注释呈现内存开销很大，因此必须限制返回的行数。 </p>
        <p>标题和文本字段支持模板化，并可以使用查询返回的数据。例如，“标题”字段可以包含以下文本：</p>
        <code>
          {`${'{{metric.type}}'}`} 有值: {`${'{{metric.value}}'}`}
        </code>
        <p>
          示例结果: <code>monitoring.googleapis.com/uptime_check/http_status 有这个值: 502</code>
        </p>
        <label>模式:</label>
        <p>
          <code>{`${'{{metric.value}}'}`}</code> = value of the metric/point
        </p>
        <p>
          <code>{`${'{{metric.type}}'}`}</code> = metric type e.g. compute.googleapis.com/instance/cpu/usage_time
        </p>
        <p>
          <code>{`${'{{metric.name}}'}`}</code> = name part of metric e.g. instance/cpu/usage_time
        </p>
        <p>
          <code>{`${'{{metric.service}}'}`}</code> = service part of metric e.g. compute
        </p>
        <p>
          <code>{`${'{{metric.label.label_name}}'}`}</code> = Metric label metadata e.g. metric.label.instance_name
        </p>
        <p>
          <code>{`${'{{resource.label.label_name}}'}`}</code> = Resource label metadata e.g. resource.label.zone
        </p>
      </div>
    </div>
  );
};
