import React, { PureComponent } from 'react';
import {
  DataSourcePluginOptionsEditorProps,
  SelectableValue,
  onUpdateDatasourceOption,
  updateDatasourcePluginResetOption,
  onUpdateDatasourceJsonDataOption,
  onUpdateDatasourceJsonDataOptionSelect,
  onUpdateDatasourceSecureJsonDataOption,
} from '@grafana/data';
import { DataSourceHttpSettings, FormLabel, Input, SecretFormField, Select } from '@grafana/ui';
import { InfluxOptions, InfluxSecureJsonData } from '../types';

const httpModes = [
  { label: 'GET', value: 'GET' },
  { label: 'POST', value: 'POST' },
] as SelectableValue[];

export type Props = DataSourcePluginOptionsEditorProps<InfluxOptions>;

export class ConfigEditor extends PureComponent<Props> {
  onResetPassword = () => {
    updateDatasourcePluginResetOption(this.props, 'password');
  };

  render() {
    const { options, onOptionsChange } = this.props;
    const { secureJsonFields } = options;
    const secureJsonData = (options.secureJsonData || {}) as InfluxSecureJsonData;
    return (
      <>
        <DataSourceHttpSettings
          showAccessOptions={true}
          dataSourceConfig={options}
          defaultUrl="http://localhost:8086"
          onChange={onOptionsChange}
        />

        <h3 className="page-heading">InfluxDB详细信息</h3>
        <div className="gf-form-group">
          <div className="gf-form-inline">
            <div className="gf-form">
              <FormLabel className="width-10">数据库</FormLabel>
              <div className="width-20">
                <Input
                  className="width-20"
                  value={options.database || ''}
                  onChange={onUpdateDatasourceOption(this.props, 'database')}
                />
              </div>
            </div>
          </div>
          <div className="gf-form-inline">
            <div className="gf-form">
              <FormLabel className="width-10">用户</FormLabel>
              <div className="width-10">
                <Input
                  className="width-20"
                  value={options.user || ''}
                  onChange={onUpdateDatasourceOption(this.props, 'user')}
                />
              </div>
            </div>
          </div>
          <div className="gf-form-inline">
            <div className="gf-form">
              <SecretFormField
                isConfigured={(secureJsonFields && secureJsonFields.password) as boolean}
                value={secureJsonData.password || ''}
                label="Password"
                labelWidth={10}
                inputWidth={20}
                onReset={this.onResetPassword}
                onChange={onUpdateDatasourceSecureJsonDataOption(this.props, 'password')}
              />
            </div>
          </div>
          <div className="gf-form-inline">
            <div className="gf-form">
              <FormLabel
                className="width-10"
                tooltip="您可以使用GET或POST HTTP方法来查询infloxdb数据库。POST方法允许您执行大量的请求（使用大量WHERE子句），而GET方法将限制您并在查询太大时返回错误。"
              >
                HTTP方式
              </FormLabel>
              <Select
                className="width-10"
                value={httpModes.find(httpMode => httpMode.value === options.jsonData.httpMode)}
                options={httpModes}
                defaultValue={options.jsonData.httpMode}
                onChange={onUpdateDatasourceJsonDataOptionSelect(this.props, 'httpMode')}
              />
            </div>
          </div>
        </div>
        <div className="gf-form-group">
          <div className="grafana-info-box">
            <h5>数据库访问</h5>
            <p>
              为此数据源设置数据库不会拒绝对其他数据库的访问。infloxdb查询语法允许在查询中切换数据库。例如:
              <code>SHOW MEASUREMENTS ON _internal</code> or <code>SELECT * FROM "_internal".."database" LIMIT 10</code>
              <br />
              <br />
              为了支持数据隔离和安全性，请确保在infloxdb中配置了适当的权限。
            </p>
          </div>
        </div>
        <div className="gf-form-group">
          <div className="gf-form-inline">
            <div className="gf-form">
              <FormLabel
                className="width-10"
                tooltip="按时间间隔自动分组的下限。建议设置为写入频率，例如，如果每分钟写入一次数据，则为1m。"
              >
                最小时间间隔
              </FormLabel>
              <div className="width-10">
                <Input
                  className="width-10"
                  placeholder="10s"
                  value={options.jsonData.timeInterval || ''}
                  onChange={onUpdateDatasourceJsonDataOption(this.props, 'timeInterval')}
                />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default ConfigEditor;
