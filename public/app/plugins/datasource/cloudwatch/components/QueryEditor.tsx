import React, { PureComponent, ChangeEvent } from 'react';
import { ExploreQueryFieldProps } from '@grafana/data';
import { Input, ValidationEvents, EventsWithValidation, Switch } from '@grafana/ui';
import { CloudWatchQuery } from '../types';
import CloudWatchDatasource from '../datasource';
import { QueryField, Alias, QueryFieldsEditor } from './';

export type Props = ExploreQueryFieldProps<CloudWatchDatasource, CloudWatchQuery>;

interface State {
  showMeta: boolean;
}

const idValidationEvents: ValidationEvents = {
  [EventsWithValidation.onBlur]: [
    {
      rule: value => new RegExp(/^$|^[a-z][a-zA-Z0-9_]*$/).test(value),
      errorMessage: 'Invalid format. Only alphanumeric characters and underscores are allowed',
    },
  ],
};

export class QueryEditor extends PureComponent<Props, State> {
  state: State = { showMeta: false };

  static getDerivedStateFromProps(props: Props, state: State) {
    const { query } = props;

    if (!query.namespace) {
      query.namespace = '';
    }

    if (!query.metricName) {
      query.metricName = '';
    }

    if (!query.expression) {
      query.expression = '';
    }

    if (!query.dimensions) {
      query.dimensions = {};
    }

    if (!query.region) {
      query.region = 'default';
    }

    if (!query.id) {
      query.id = '';
    }

    if (!query.alias) {
      query.alias = '';
    }

    if (!query.statistics || !query.statistics.length) {
      query.statistics = ['Average'];
    }

    if (!query.hasOwnProperty('matchExact')) {
      query.matchExact = true;
    }

    return state;
  }

  onChange(query: CloudWatchQuery) {
    const { onChange, onRunQuery } = this.props;
    onChange(query);
    onRunQuery();
  }

  render() {
    const { data, query, onRunQuery } = this.props;
    const { showMeta } = this.state;
    const metaDataExist = data && Object.values(data).length && data.state === 'Done';
    return (
      <>
        <QueryFieldsEditor {...this.props}></QueryFieldsEditor>
        {query.statistics.length <= 1 && (
          <div className="gf-form-inline">
            <div className="gf-form">
              <QueryField label="Id" tooltip="Id可以包含数字、字母和下划线，并且必须以小写字母开头。">
                <Input
                  className="gf-form-input width-8"
                  onBlur={onRunQuery}
                  onChange={(event: ChangeEvent<HTMLInputElement>) =>
                    this.onChange({ ...query, id: event.target.value })
                  }
                  validationEvents={idValidationEvents}
                  value={query.id || ''}
                />
              </QueryField>
            </div>
            <div className="gf-form gf-form--grow">
              <QueryField
                className="gf-form--grow"
                label="表达式"
                tooltip="也可以在此处添加表达式。请注意，如果正在使用引用其他查询的数学表达式，则无法基于此查询创建告警规则"
              >
                <Input
                  className="gf-form-input"
                  onBlur={onRunQuery}
                  value={query.expression || ''}
                  onChange={(event: ChangeEvent<HTMLInputElement>) =>
                    this.onChange({ ...query, expression: event.target.value })
                  }
                />
              </QueryField>
            </div>
          </div>
        )}
        <div className="gf-form-inline">
          <div className="gf-form">
            <QueryField label="期间" tooltip="点之间的最小间隔（秒）">
              <Input
                className="gf-form-input width-8"
                value={query.period || ''}
                placeholder="自动"
                onBlur={onRunQuery}
                onChange={(event: ChangeEvent<HTMLInputElement>) =>
                  this.onChange({ ...query, period: event.target.value })
                }
              />
            </QueryField>
          </div>
          <div className="gf-form">
            <QueryField
              label="别名"
              tooltip="别名替换变量: {{metric}}, {{stat}}, {{namespace}}, {{region}}, {{period}}, {{label}}, {{YOUR_DIMENSION_NAME}}"
            >
              <Alias value={query.alias} onChange={(value: string) => this.onChange({ ...query, alias: value })} />
            </QueryField>
            <Switch
              label="精确匹配"
              labelClass="query-keyword"
              tooltip="只显示与所有定义的维度名称完全匹配的度量。"
              checked={query.matchExact}
              onChange={() => this.onChange({ ...query, matchExact: !query.matchExact })}
            />
            <label className="gf-form-label">
              <a
                onClick={() =>
                  metaDataExist &&
                  this.setState({
                    showMeta: !showMeta,
                  })
                }
              >
                <i className={`fa fa-caret-${showMeta ? 'down' : 'right'}`} /> {showMeta ? 'Hide' : 'Show'} 查询 预览
              </a>
            </label>
          </div>
          <div className="gf-form gf-form--grow">
            <div className="gf-form-label gf-form-label--grow" />
          </div>
          {showMeta && metaDataExist && (
            <table className="filter-table form-inline">
              <thead>
                <tr>
                  <th>度量数据查询ID</th>
                  <th>度量数据查询表达式</th>
                  <th>期间</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {data.series[0].meta.gmdMeta.map(({ ID, Expression, Period }: any) => (
                  <tr key={ID}>
                    <td>{ID}</td>
                    <td>{Expression}</td>
                    <td>{Period}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </div>
      </>
    );
  }
}
