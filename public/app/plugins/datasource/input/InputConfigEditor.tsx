// Libraries
import React, { PureComponent } from 'react';

// Types
import { InputOptions } from './types';

import { TableInputCSV } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps, DataFrame, MutableDataFrame } from '@grafana/data';
import { dataFrameToCSV } from './utils';

interface Props extends DataSourcePluginOptionsEditorProps<InputOptions> {}

interface State {
  text: string;
}

export class InputConfigEditor extends PureComponent<Props, State> {
  state = {
    text: '',
  };

  componentDidMount() {
    const { options } = this.props;
    if (options.jsonData.data) {
      const text = dataFrameToCSV(options.jsonData.data);
      this.setState({ text });
    }
  }

  onSeriesParsed = (data: DataFrame[], text: string) => {
    const { options, onOptionsChange } = this.props;
    if (!data) {
      data = [new MutableDataFrame()];
    }
    // data is a property on 'jsonData'
    const jsonData = {
      ...options.jsonData,
      data,
    };

    onOptionsChange({
      ...options,
      jsonData,
    });
    this.setState({ text });
  };

  render() {
    const { text } = this.state;
    return (
      <div>
        <div className="gf-form-group">
          <h4>共享数据:</h4>
          <span>输入CSV</span>
          <TableInputCSV text={text} onSeriesParsed={this.onSeriesParsed} width={'100%'} height={200} />
        </div>

        <div className="grafana-info-box">
          这些数据存储在数据源json中，并在任何数据源的初始请求中返回给每个用户。这是输入一些值的适当位置。大型数据集在其他数据源中的性能会更好。
          <br />
          <br />
          <b>注:</b> 只有在刷新浏览器后才会反映对此数据的更改。
        </div>
      </div>
    );
  }
}
