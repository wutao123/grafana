import { FieldPropertyEditorItem, Registry, FieldConfigEditorRegistry } from '@grafana/data';
import {
  NumberValueEditor,
  NumberOverrideEditor,
  numberOverrideProcessor,
  NumberFieldConfigSettings,
  selectOverrideProcessor,
  SelectValueEditor,
  SelectOverrideEditor,
  SelectFieldConfigSettings,
} from '@grafana/ui';

export const tableFieldRegistry: FieldConfigEditorRegistry = new Registry<FieldPropertyEditorItem>(() => {
  const columWidth: FieldPropertyEditorItem<number, NumberFieldConfigSettings> = {
    id: 'width', // Match field properties
    name: 'Column width',
    description: '列宽（用于表格）',

    editor: NumberValueEditor,
    override: NumberOverrideEditor,
    process: numberOverrideProcessor,

    settings: {
      placeholder: '自动',
      min: 20,
      max: 300,
    },

    shouldApply: () => true,
  };

  const cellDisplayMode: FieldPropertyEditorItem<string, SelectFieldConfigSettings<string>> = {
    id: 'displayMode', // Match field properties
    name: '单元显示模式',
    description: '颜色值、背景、显示为仪表等',

    editor: SelectValueEditor,
    override: SelectOverrideEditor,
    process: selectOverrideProcessor,

    settings: {
      options: [
        { value: 'auto', label: '自动' },
        { value: 'color-background', label: '颜色背景' },
        { value: 'gradient-gauge', label: '渐变仪表' },
        { value: 'lcd-gauge', label: 'LCD仪表' },
      ],
    },

    shouldApply: () => true,
  };

  return [columWidth, cellDisplayMode];
});
