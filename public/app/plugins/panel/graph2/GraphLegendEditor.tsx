import React from 'react';
import { LegendOptions, PanelOptionsGroup, Switch, Input, StatsPicker } from '@grafana/ui';

export interface GraphLegendEditorLegendOptions extends LegendOptions {
  stats?: string[];
  decimals?: number;
  sortBy?: string;
  sortDesc?: boolean;
}

interface GraphLegendEditorProps {
  options: GraphLegendEditorLegendOptions;
  onChange: (options: GraphLegendEditorLegendOptions) => void;
}

export const GraphLegendEditor: React.FunctionComponent<GraphLegendEditorProps> = props => {
  const { options, onChange } = props;

  const onStatsChanged = (stats: string[]) => {
    onChange({
      ...options,
      stats,
    });
  };

  const onOptionToggle = (option: keyof LegendOptions) => (event?: React.ChangeEvent<HTMLInputElement>) => {
    const newOption: Partial<LegendOptions> = {};
    if (!event) {
      return;
    }

    if (option === 'placement') {
      newOption[option] = event.target.checked ? 'right' : 'under';
    } else {
      newOption[option] = event.target.checked;
    }

    onChange({
      ...options,
      ...newOption,
    });
  };

  const labelWidth = 8;
  return (
    <PanelOptionsGroup title="图例">
      <div className="section gf-form-group">
        <h4>选项</h4>
        <Switch
          label="显示图例"
          labelClass={`width-${labelWidth}`}
          checked={options.isVisible}
          onChange={onOptionToggle('isVisible')}
        />
        <Switch
          label="显示为表格"
          labelClass={`width-${labelWidth}`}
          checked={options.asTable}
          onChange={onOptionToggle('asTable')}
        />
        <Switch
          label="向右"
          labelClass={`width-${labelWidth}`}
          checked={options.placement === 'right'}
          onChange={onOptionToggle('placement')}
        />
      </div>

      <div className="section gf-form-group">
        <h4>显示</h4>
        <div className="gf-form">
          <StatsPicker
            allowMultiple={true}
            stats={options.stats ? options.stats : []}
            onChange={onStatsChanged}
            placeholder={'选取值'}
          />
        </div>

        <div className="gf-form">
          <div className="gf-form-label">小数位</div>
          <Input
            className="gf-form-input width-5"
            type="number"
            value={options.decimals}
            placeholder="自动"
            onChange={event => {
              onChange({
                ...options,
                decimals: parseInt(event.target.value, 10),
              });
            }}
          />
        </div>
      </div>

      <div className="section gf-form-group">
        <h4>隐藏序列</h4>
        {/* <Switch label="With only nulls" checked={!!options.hideEmpty} onChange={onOptionToggle('hideEmpty')} /> */}
        <Switch label="只有零值" checked={!!options.hideZero} onChange={onOptionToggle('hideZero')} />
      </div>
    </PanelOptionsGroup>
  );
};
