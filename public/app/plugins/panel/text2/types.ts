export type TextMode = 'html' | 'markdown' | 'text';
export interface TextOptions {
  mode: TextMode;
  content: string;
}

export const defaults: TextOptions = {
  mode: 'markdown',
  content: `# 标题

  markdown语法帮助: [commonmark.org/help](https://commonmark.org/help/)


`,
};
