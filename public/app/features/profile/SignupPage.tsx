import React, { FC } from 'react';
import { SignupForm } from './SignupForm';
import Page from 'app/core/components/Page/Page';
import { getConfig } from 'app/core/config';
import { connect } from 'react-redux';
import { hot } from 'react-hot-loader';
import { StoreState } from 'app/types';

const navModel = {
  main: {
    icon: 'gicon gicon-branding',
    text: '注册',
    subTitle: '注册您的Grafana账户',
    breadcrumbs: [{ title: '登录', url: 'login' }],
  },
  node: {
    text: '',
  },
};

interface Props {
  email?: string;
  orgName?: string;
  username?: string;
  code?: string;
  name?: string;
}
export const SignupPage: FC<Props> = props => {
  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <h3 className="p-b-1">你就快到了。</h3>
        <div className="p-b-1">
          我们需要更多的
          <br /> 信息去完成创建帐户。
        </div>
        <SignupForm
          {...props}
          verifyEmailEnabled={getConfig().verifyEmailEnabled}
          autoAssignOrg={getConfig().autoAssignOrg}
        />
      </Page.Contents>
    </Page>
  );
};

const mapStateToProps = (state: StoreState) => ({
  ...state.location.routeParams,
});

export default hot(module)(connect(mapStateToProps)(SignupPage));
