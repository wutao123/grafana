import React, { MouseEvent, PureComponent } from 'react';
import { e2e } from '@grafana/e2e';
import EmptyListCTA from '../../../core/components/EmptyListCTA/EmptyListCTA';
import { QueryVariableModel, VariableModel } from '../variable';
import { toVariableIdentifier, VariableIdentifier } from '../state/types';

export interface Props {
  variables: VariableModel[];
  onAddClick: (event: MouseEvent<HTMLAnchorElement>) => void;
  onEditClick: (identifier: VariableIdentifier) => void;
  onChangeVariableOrder: (identifier: VariableIdentifier, fromIndex: number, toIndex: number) => void;
  onDuplicateVariable: (identifier: VariableIdentifier) => void;
  onRemoveVariable: (identifier: VariableIdentifier) => void;
}

enum MoveType {
  down = 1,
  up = -1,
}

export class VariableEditorList extends PureComponent<Props> {
  onEditClick = (event: MouseEvent, identifier: VariableIdentifier) => {
    event.preventDefault();
    this.props.onEditClick(identifier);
  };

  onChangeVariableOrder = (event: MouseEvent, variable: VariableModel, moveType: MoveType) => {
    event.preventDefault();
    this.props.onChangeVariableOrder(toVariableIdentifier(variable), variable.index!, variable.index! + moveType);
  };

  onDuplicateVariable = (event: MouseEvent, identifier: VariableIdentifier) => {
    event.preventDefault();
    this.props.onDuplicateVariable(identifier);
  };

  onRemoveVariable = (event: MouseEvent, identifier: VariableIdentifier) => {
    event.preventDefault();
    this.props.onRemoveVariable(identifier);
  };

  render() {
    return (
      <div>
        <div>
          {this.props.variables.length === 0 && (
            <div>
              <EmptyListCTA
                title="还没有变量"
                buttonIcon="gicon gicon-variable"
                buttonTitle="添加变量"
                infoBox={{
                  __html: ` <p>
                    变量使仪表板更具交互性和动态性。与在测量指标查询中硬编码诸如服务器或传感器名称之类的内容不同，您可以使用变量代替它们。变量显示为仪表板顶部的下拉选择框。这些下拉菜单使您可以轻松更改仪表板中显示的数据。查看
                    <a class="external-link" href="http://docs.grafana.org/reference/templating/" target="_blank">
                      模板文档
                    </a>
                    更多信息。
                  </p>`,
                }}
                infoBoxTitle="变量的作用是什么？"
                onClick={this.props.onAddClick}
              />
            </div>
          )}

          {this.props.variables.length > 0 && (
            <div>
              <table
                className="filter-table filter-table--hover"
                aria-label={e2e.pages.Dashboard.Settings.Variables.List.selectors.table}
              >
                <thead>
                  <tr>
                    <th>变量</th>
                    <th>定义</th>
                    <th colSpan={5} />
                  </tr>
                </thead>
                <tbody>
                  {this.props.variables.map((state, index) => {
                    const variable = state as QueryVariableModel;
                    return (
                      <tr key={`${variable.name}-${index}`}>
                        <td style={{ width: '1%' }}>
                          <span
                            onClick={event => this.onEditClick(event, toVariableIdentifier(variable))}
                            className="pointer template-variable"
                            aria-label={e2e.pages.Dashboard.Settings.Variables.List.selectors.tableRowNameFields(
                              variable.name
                            )}
                          >
                            {variable.name}
                          </span>
                        </td>
                        <td
                          style={{ maxWidth: '200px' }}
                          onClick={event => this.onEditClick(event, toVariableIdentifier(variable))}
                          className="pointer max-width"
                          aria-label={e2e.pages.Dashboard.Settings.Variables.List.selectors.tableRowDefinitionFields(
                            variable.name
                          )}
                        >
                          {variable.definition ? variable.definition : variable.query}
                        </td>

                        <td style={{ width: '1%' }}>
                          {index > 0 && (
                            <i
                              onClick={event => this.onChangeVariableOrder(event, variable, MoveType.up)}
                              className="pointer fa fa-arrow-up"
                              aria-label={e2e.pages.Dashboard.Settings.Variables.List.selectors.tableRowArrowUpButtons(
                                variable.name
                              )}
                            />
                          )}
                        </td>
                        <td style={{ width: '1%' }}>
                          {index < this.props.variables.length - 1 && (
                            <i
                              onClick={event => this.onChangeVariableOrder(event, variable, MoveType.down)}
                              className="pointer fa fa-arrow-down"
                              aria-label={e2e.pages.Dashboard.Settings.Variables.List.selectors.tableRowArrowDownButtons(
                                variable.name
                              )}
                            />
                          )}
                        </td>
                        <td style={{ width: '1%' }}>
                          <a
                            onClick={event => this.onDuplicateVariable(event, toVariableIdentifier(variable))}
                            className="btn btn-inverse btn-small"
                            aria-label={e2e.pages.Dashboard.Settings.Variables.List.selectors.tableRowDuplicateButtons(
                              variable.name
                            )}
                          >
                            复制
                          </a>
                        </td>
                        <td style={{ width: '1%' }}>
                          <a
                            onClick={event => this.onRemoveVariable(event, toVariableIdentifier(variable))}
                            className="btn btn-danger btn-small"
                            aria-label={e2e.pages.Dashboard.Settings.Variables.List.selectors.tableRowRemoveButtons(
                              variable.name
                            )}
                          >
                            <i className="fa fa-remove" />
                          </a>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
    );
  }
}
