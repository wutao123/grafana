import _ from 'lodash';
import { AppEvents } from '@grafana/data';
import { e2e } from '@grafana/e2e';

import coreModule from 'app/core/core_module';
import { variableTypes } from './variable';
import appEvents from 'app/core/app_events';
import DatasourceSrv from '../plugins/datasource_srv';
import { VariableSrv } from './all';
import { TemplateSrv } from './template_srv';
import { promiseToDigest } from '../../core/utils/promiseToDigest';

export class VariableEditorCtrl {
  /** @ngInject */
  constructor($scope: any, datasourceSrv: DatasourceSrv, variableSrv: VariableSrv, templateSrv: TemplateSrv) {
    $scope.variableTypes = variableTypes;
    $scope.ctrl = {};
    $scope.namePattern = /^(?!__).*$/;
    $scope._ = _;
    $scope.optionsLimit = 20;
    $scope.emptyListCta = {
      title: '还没有变量',
      buttonTitle: '添加变量',
      buttonIcon: 'gicon gicon-variable',
      infoBox: {
        __html: ` <p>
      变量使仪表板更具交互性和动态性。与在度量查询中硬编码诸如服务器或传感器名称之类的内容不同，您可以使用变量代替它们。变量显示为仪表板顶部的下拉选择框。这些下拉菜单使您可以轻松更改仪表板中显示的数据。查看
      <a class="external-link" href="http://docs.grafana.org/reference/templating/" target="_blank">
        模板文档
      </a>
      了解更多信息。
    </p>`,
        infoBoxTitle: '变量的作用是什么？',
      },
    };

    $scope.refreshOptions = [
      { value: 0, text: '从不' },
      { value: 1, text: '仪表板负载' },
      { value: 2, text: '时间范围变化' },
    ];

    $scope.sortOptions = [
      { value: 0, text: '禁用' },
      { value: 1, text: '字母（升序）' },
      { value: 2, text: '字母（降序）' },
      { value: 3, text: '数字（升序）' },
      { value: 4, text: '数字（降序）' },
      { value: 5, text: '字母顺序（不区分大小写，升序）' },
      { value: 6, text: '字母顺序（不区分大小写，升序）' },
    ];

    $scope.hideOptions = [
      { value: 0, text: '' },
      { value: 1, text: '标签' },
      { value: 2, text: '变量' },
    ];

    $scope.selectors = {
      ...e2e.pages.Dashboard.Settings.Variables.List.selectors,
      ...e2e.pages.Dashboard.Settings.Variables.Edit.General.selectors,
      ...e2e.pages.Dashboard.Settings.Variables.Edit.QueryVariable.selectors,
      ...e2e.pages.Dashboard.Settings.Variables.Edit.ConstantVariable.selectors,
    };

    $scope.init = () => {
      $scope.mode = 'list';

      $scope.variables = variableSrv.variables;
      $scope.reset();

      $scope.$watch('mode', (val: string) => {
        if (val === 'new') {
          $scope.reset();
        }
      });
    };

    $scope.setMode = (mode: any) => {
      $scope.mode = mode;
    };

    $scope.setNewMode = () => {
      $scope.setMode('new');
    };

    $scope.add = () => {
      if ($scope.isValid()) {
        variableSrv.addVariable($scope.current);
        $scope.update();
      }
    };

    $scope.isValid = () => {
      if (!$scope.ctrl.form.$valid) {
        return false;
      }

      if (!$scope.current.name.match(/^\w+$/)) {
        appEvents.emit(AppEvents.alertWarning, [
          'Validation',
          'Only word and digit characters are allowed in variable names',
        ]);
        return false;
      }

      const sameName: any = _.find($scope.variables, { name: $scope.current.name });
      if (sameName && sameName !== $scope.current) {
        appEvents.emit(AppEvents.alertWarning, ['Validation', 'Variable with the same name already exists']);
        return false;
      }

      if (
        $scope.current.type === 'query' &&
        _.isString($scope.current.query) &&
        $scope.current.query.match(new RegExp('\\$' + $scope.current.name + '(/| |$)'))
      ) {
        appEvents.emit(AppEvents.alertWarning, [
          'Validation',
          'Query cannot contain a reference to itself. Variable: $' + $scope.current.name,
        ]);
        return false;
      }

      return true;
    };

    $scope.validate = () => {
      $scope.infoText = '';
      if ($scope.current.type === 'adhoc' && $scope.current.datasource !== null) {
        $scope.infoText = 'Adhoc filters are applied automatically to all queries that target this datasource';
        promiseToDigest($scope)(
          datasourceSrv.get($scope.current.datasource).then(ds => {
            if (!ds.getTagKeys) {
              $scope.infoText = 'This datasource does not support adhoc filters yet.';
            }
          })
        );
      }
    };

    $scope.runQuery = () => {
      $scope.optionsLimit = 20;
      return variableSrv.updateOptions($scope.current).catch((err: { data: { message: any }; message: string }) => {
        if (err.data && err.data.message) {
          err.message = err.data.message;
        }
        appEvents.emit(AppEvents.alertError, [
          'Templating',
          'Template variables could not be initialized: ' + err.message,
        ]);
      });
    };

    $scope.onQueryChange = (query: any, definition: any) => {
      $scope.current.query = query;
      $scope.current.definition = definition;
      $scope.runQuery();
    };

    $scope.edit = (variable: any) => {
      $scope.current = variable;
      $scope.currentIsNew = false;
      $scope.mode = 'edit';
      $scope.validate();
      promiseToDigest($scope)(
        datasourceSrv.get($scope.current.datasource).then(ds => {
          $scope.currentDatasource = ds;
        })
      );
    };

    $scope.duplicate = (variable: { getSaveModel: () => void; name: string }) => {
      const clone = _.cloneDeep(variable.getSaveModel());
      $scope.current = variableSrv.createVariableFromModel(clone);
      $scope.current.name = 'copy_of_' + variable.name;
      variableSrv.addVariable($scope.current);
    };

    $scope.update = () => {
      if ($scope.isValid()) {
        promiseToDigest($scope)(
          $scope.runQuery().then(() => {
            $scope.reset();
            $scope.mode = 'list';
            templateSrv.updateIndex();
          })
        );
      }
    };

    $scope.reset = () => {
      $scope.currentIsNew = true;
      $scope.current = variableSrv.createVariableFromModel({ type: 'query' });

      // this is done here in case a new data source type variable was added
      $scope.datasources = _.filter(datasourceSrv.getMetricSources(), ds => {
        return !ds.meta.mixed && ds.value !== null;
      });

      $scope.datasourceTypes = _($scope.datasources)
        .uniqBy('meta.id')
        .map((ds: any) => {
          return { text: ds.meta.name, value: ds.meta.id };
        })
        .value();
    };

    $scope.typeChanged = function() {
      const old = $scope.current;
      $scope.current = variableSrv.createVariableFromModel({
        type: $scope.current.type,
      });
      $scope.current.name = old.name;
      $scope.current.label = old.label;

      const oldIndex = _.indexOf(this.variables, old);
      if (oldIndex !== -1) {
        this.variables[oldIndex] = $scope.current;
      }

      $scope.validate();
    };

    $scope.removeVariable = (variable: any) => {
      variableSrv.removeVariable(variable);
    };

    $scope.showMoreOptions = () => {
      $scope.optionsLimit += 20;
    };

    $scope.datasourceChanged = async () => {
      promiseToDigest($scope)(
        datasourceSrv.get($scope.current.datasource).then(ds => {
          $scope.current.query = '';
          $scope.currentDatasource = ds;
        })
      );
    };
  }
}

coreModule.controller('VariableEditorCtrl', VariableEditorCtrl);
