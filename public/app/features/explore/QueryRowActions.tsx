import React from 'react';

function formatLatency(value: number) {
  return `${(value / 1000).toFixed(1)}s`;
}

export type Props = {
  canToggleEditorModes: boolean;
  isDisabled?: boolean;
  isNotStarted: boolean;
  latency: number;
  onClickToggleEditorMode: () => void;
  onClickToggleDisabled: () => void;
  onClickRemoveButton: () => void;
};

export function QueryRowActions(props: Props) {
  const {
    canToggleEditorModes,
    onClickToggleEditorMode,
    onClickToggleDisabled,
    onClickRemoveButton,
    isDisabled,
    isNotStarted,
    latency,
  } = props;

  return (
    <div className="gf-form-inline flex-shrink-0">
      {canToggleEditorModes && (
        <div className="gf-form">
          <button
            aria-label="编辑模式按钮"
            className="gf-form-label gf-form-label--btn"
            onClick={onClickToggleEditorMode}
          >
            <i className="fa fa-pencil" />
          </button>
        </div>
      )}
      <div className="gf-form">
        <button disabled className="gf-form-label" title="查询行延迟">
          {formatLatency(latency)}
        </button>
      </div>
      <div className="gf-form">
        <button
          disabled={isNotStarted}
          className="gf-form-label gf-form-label--btn"
          onClick={onClickToggleDisabled}
          title="禁用/启用查询"
        >
          <i className={isDisabled ? 'fa fa-eye-slash' : 'fa fa-eye'} />
        </button>
      </div>
      <div className="gf-form">
        <button className="gf-form-label gf-form-label--btn" onClick={onClickRemoveButton} title="移除查询">
          <i className="fa fa-minus" />
        </button>
      </div>
    </div>
  );
}
