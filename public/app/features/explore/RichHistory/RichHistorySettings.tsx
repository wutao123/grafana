import React from 'react';
import { css } from 'emotion';
import { stylesFactory, useTheme, Forms } from '@grafana/ui';
import { GrafanaTheme, AppEvents } from '@grafana/data';
import appEvents from 'app/core/app_events';
import { CoreEvents } from 'app/types';

export interface RichHistorySettingsProps {
  retentionPeriod: number;
  starredTabAsFirstTab: boolean;
  activeDatasourceOnly: boolean;
  onChangeRetentionPeriod: (option: { label: string; value: number }) => void;
  toggleStarredTabAsFirstTab: () => void;
  toggleactiveDatasourceOnly: () => void;
  deleteRichHistory: () => void;
}

const getStyles = stylesFactory((theme: GrafanaTheme) => {
  return {
    container: css`
      padding-left: ${theme.spacing.sm};
      font-size: ${theme.typography.size.sm};
      .space-between {
        margin-bottom: ${theme.spacing.lg};
      }
    `,
    input: css`
      max-width: 200px;
    `,
    switch: css`
      display: flex;
      align-items: center;
    `,
    label: css`
      margin-left: ${theme.spacing.md};
    `,
  };
});

const retentionPeriodOptions = [
  { value: 2, label: '2 days' },
  { value: 5, label: '5 days' },
  { value: 7, label: '1 week' },
  { value: 14, label: '2 weeks' },
];

export function RichHistorySettings(props: RichHistorySettingsProps) {
  const {
    retentionPeriod,
    starredTabAsFirstTab,
    activeDatasourceOnly,
    onChangeRetentionPeriod,
    toggleStarredTabAsFirstTab,
    toggleactiveDatasourceOnly,
    deleteRichHistory,
  } = props;
  const theme = useTheme();
  const styles = getStyles(theme);
  const selectedOption = retentionPeriodOptions.find(v => v.value === retentionPeriod);

  const onDelete = () => {
    appEvents.emit(CoreEvents.showConfirmModal, {
      title: '删除',
      text: '确实要永久删除查询历史记录吗？',
      yesText: '删除',
      icon: 'fa-trash',
      onConfirm: () => {
        deleteRichHistory();
        appEvents.emit(AppEvents.alertSuccess, ['Query history deleted']);
      },
    });
  };

  return (
    <div className={styles.container}>
      <Forms.Field
        label="历史时间跨度"
        description="Select the period of time for which Grafana will save your query history"
        className="space-between"
      >
        <div className={styles.input}>
          <Forms.Select
            value={selectedOption}
            options={retentionPeriodOptions}
            onChange={onChangeRetentionPeriod}
          ></Forms.Select>
        </div>
      </Forms.Field>
      <Forms.Field label="默认活动选项卡" description=" " className="space-between">
        <div className={styles.switch}>
          <Forms.Switch value={starredTabAsFirstTab} onChange={toggleStarredTabAsFirstTab}></Forms.Switch>
          <div className={styles.label}>将默认活动选项卡从“查询历史记录”更改为“星号”</div>
        </div>
      </Forms.Field>
      <Forms.Field label="数据源行为" description=" " className="space-between">
        <div className={styles.switch}>
          <Forms.Switch value={activeDatasourceOnly} onChange={toggleactiveDatasourceOnly}></Forms.Switch>
          <div className={styles.label}>仅显示对Explore中当前活动数据源的查询</div>
        </div>
      </Forms.Field>
      <div
        className={css`
          font-weight: ${theme.typography.weight.bold};
        `}
      >
        Clear query history
      </div>
      <div
        className={css`
          margin-bottom: ${theme.spacing.sm};
        `}
      >
        永久删除所有查询历史记录。
      </div>
      <Forms.Button variant="destructive" onClick={onDelete}>
        清除查询历史记录
      </Forms.Button>
    </div>
  );
}
