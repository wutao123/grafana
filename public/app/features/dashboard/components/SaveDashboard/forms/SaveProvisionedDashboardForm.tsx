import React, { useCallback, useMemo } from 'react';
import { css } from 'emotion';
import { saveAs } from 'file-saver';
import { CustomScrollbar, Forms, Button, HorizontalGroup, JSONFormatter, VerticalGroup } from '@grafana/ui';
import { CopyToClipboard } from 'app/core/components/CopyToClipboard/CopyToClipboard';
import { SaveDashboardFormProps } from '../types';

export const SaveProvisionedDashboardForm: React.FC<SaveDashboardFormProps> = ({ dashboard, onCancel }) => {
  const dashboardJSON = useMemo(() => {
    const clone = dashboard.getSaveModelClone();
    delete clone.id;
    return clone;
  }, [dashboard]);

  const getClipboardText = useCallback(() => {
    return JSON.stringify(dashboardJSON, null, 2);
  }, [dashboard]);

  const saveToFile = useCallback(() => {
    const blob = new Blob([JSON.stringify(dashboardJSON, null, 2)], {
      type: 'application/json;charset=utf-8',
    });
    saveAs(blob, dashboard.title + '-' + new Date().getTime() + '.json');
  }, [dashboardJSON]);

  return (
    <>
      <VerticalGroup spacing="lg">
        <small>
          无法从Grafana的UI保存此仪表板，因为它是从其他源设置的。复制JSON或将其保存到下面的文件中。然后可以在相应的配置源中更新仪表板。
          <br />
          <i>
            查看{' '}
            <a
              className="external-link"
              href="http://docs.grafana.org/administration/provisioning/#dashboards"
              target="_blank"
            >
              文档
            </a>{' '}
            有关资源调配的详细信息。
          </i>
        </small>
        <div>
          <strong>文件路径: </strong> {dashboard.meta.provisionedExternalId}
        </div>
        <div
          className={css`
            padding: 8px 16px;
            background: black;
            height: 400px;
          `}
        >
          <CustomScrollbar>
            <JSONFormatter json={dashboardJSON} open={1} />
          </CustomScrollbar>
        </div>
        <HorizontalGroup>
          <CopyToClipboard text={getClipboardText} elType={Button}>
            将JSON复制到剪贴板
          </CopyToClipboard>
          <Button onClick={saveToFile}>将JSON保存到文件</Button>
          <Forms.Button variant="secondary" onClick={onCancel}>
            取消
          </Forms.Button>
        </HorizontalGroup>
      </VerticalGroup>
    </>
  );
};
