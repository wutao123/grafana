export interface PanelEditorTab {
  id: string;
  text: string;
  active: boolean;
}

export enum PanelEditorTabId {
  Queries = 'queries',
  Transform = 'transform',
  Visualization = 'visualization',
  Alert = 'alert',
}

export enum DisplayMode {
  Fill = 0,
  Fit = 1,
  Exact = 2,
}

export const displayModes = [
  { value: DisplayMode.Fill, label: '填充', description: '使用所有可用空间' },
  { value: DisplayMode.Fit, label: '符合', description: '符合空间保持率' },
  { value: DisplayMode.Exact, label: '准确的', description: '与仪表板大小相同' },
];
