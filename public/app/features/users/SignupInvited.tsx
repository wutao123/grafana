import React, { FC, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { StoreState } from 'app/types';
import { updateLocation } from 'app/core/actions';
import { UrlQueryValue, getBackendSrv } from '@grafana/runtime';
import { Forms } from '@grafana/ui';
import { useAsync } from 'react-use';
import Page from 'app/core/components/Page/Page';
import { contextSrv } from 'app/core/core';
import { getConfig } from 'app/core/config';

interface ConnectedProps {
  code?: UrlQueryValue;
}

interface DispatchProps {
  updateLocation: typeof updateLocation;
}

interface FormModel {
  email: string;
  name?: string;
  username: string;
  password?: string;
}

const navModel = {
  main: {
    icon: 'gicon gicon-branding',
    text: '邀请',
    subTitle: '注册您的Grafana帐户',
    breadcrumbs: [{ title: '登录', url: 'login' }],
  },
  node: {
    text: '',
  },
};

const SingupInvitedPageUnconnected: FC<DispatchProps & ConnectedProps> = ({ code }) => {
  const [initFormModel, setInitFormModel] = useState<FormModel>();
  const [greeting, setGreeting] = useState<string>();
  const [invitedBy, setInvitedBy] = useState<string>();
  useAsync(async () => {
    const invite = await getBackendSrv().get('/api/user/invite/' + code);
    setInitFormModel({
      email: invite.email,
      name: invite.name,
      username: invite.email,
    });

    setGreeting(invite.name || invite.email || invite.username);
    setInvitedBy(invite.invitedBy);
  }, []);

  const onSubmit = async (formData: FormModel) => {
    await getBackendSrv().post('/api/user/invite/complete', { ...formData, inviteCode: code });
    window.location.href = getConfig().appSubUrl + '/';
  };

  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <h3 className="page-sub-heading">您好 {greeting || 'there'}.</h3>

        <div className="modal-tagline p-b-2">
          <em>{invitedBy || 'Someone'}</em> 邀请你加入Grafana和组织{' '}
          <span className="highlight-word">{contextSrv.user.orgName}</span>
          <br />
          请完成以下操作并选择一个密码以接受您的邀请并继续:
        </div>
        <Forms.Form defaultValues={initFormModel} onSubmit={onSubmit}>
          {({ register, errors }) => (
            <>
              <Forms.Field invalid={!!errors.email} error={!!errors.email && errors.email.message} label="电子邮件">
                <Forms.Input
                  size="md"
                  placeholder="email@example.com"
                  name="email"
                  ref={register({
                    required: '电子邮件是必需的',
                    pattern: {
                      value: /^\S+@\S+$/,
                      message: 'Email is invalid',
                    },
                  })}
                />
              </Forms.Field>
              <Forms.Field invalid={!!errors.name} error={!!errors.name && errors.name.message} label="名字">
                <Forms.Input size="md" placeholder="名称（可选）" name="name" ref={register} />
              </Forms.Field>
              <Forms.Field
                invalid={!!errors.username}
                error={!!errors.username && errors.username.message}
                label="用户名"
              >
                <Forms.Input
                  size="md"
                  placeholder="用户名"
                  name="用户名"
                  ref={register({ required: '用户名是必需的' })}
                />
              </Forms.Field>
              <Forms.Field
                invalid={!!errors.password}
                error={!!errors.password && errors.password.message}
                label="密码"
              >
                <Forms.Input
                  size="md"
                  type="password"
                  placeholder="密码"
                  name="密码"
                  ref={register({ required: '密码是必需的' })}
                />
              </Forms.Field>

              <Forms.Button type="submit">注册</Forms.Button>
            </>
          )}
        </Forms.Form>
      </Page.Contents>
    </Page>
  );
};

const mapStateToProps: MapStateToProps<ConnectedProps, {}, StoreState> = (state: StoreState) => ({
  code: state.location.routeParams.code,
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = {
  updateLocation,
};

export default hot(module)(connect(mapStateToProps, mapDispatchToProps)(SingupInvitedPageUnconnected));
