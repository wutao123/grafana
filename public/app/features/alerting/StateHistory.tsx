import React, { PureComponent } from 'react';
import { getBackendSrv } from '@grafana/runtime';

import alertDef from './state/alertDef';
import { DashboardModel } from '../dashboard/state/DashboardModel';
import appEvents from '../../core/app_events';
import { CoreEvents } from 'app/types';

interface Props {
  dashboard: DashboardModel;
  panelId: number;
  onRefresh: () => void;
}

interface State {
  stateHistoryItems: any[];
}

class StateHistory extends PureComponent<Props, State> {
  state: State = {
    stateHistoryItems: [],
  };

  componentDidMount(): void {
    const { dashboard, panelId } = this.props;

    getBackendSrv()
      .get(
        `/api/annotations?dashboardId=${dashboard.id}&panelId=${panelId}&limit=50&type=alert`,
        {},
        `state-history-${dashboard.id}-${panelId}`
      )
      .then(data => {
        const items = data.map((item: any) => {
          return {
            stateModel: alertDef.getStateDisplayModel(item.newState),
            time: dashboard.formatDate(item.time, 'MMM D, YYYY HH:mm:ss'),
            info: alertDef.getAlertAnnotationInfo(item),
          };
        });

        this.setState({
          stateHistoryItems: items,
        });
      });
  }

  clearHistory = () => {
    const { dashboard, onRefresh, panelId } = this.props;

    appEvents.emit(CoreEvents.showConfirmModal, {
      title: '删除告警历史',
      text: '确定要删除此告警的所有历史记录和批注吗？',
      icon: 'fa-trash',
      yesText: '是的',
      onConfirm: () => {
        getBackendSrv()
          .post('/api/annotations/mass-delete', {
            dashboardId: dashboard.id,
            panelId: panelId,
          })
          .then(() => {
            this.setState({
              stateHistoryItems: [],
            });
            onRefresh();
          });
      },
    });
  };

  render() {
    const { stateHistoryItems } = this.state;

    return (
      <div>
        {stateHistoryItems.length > 0 && (
          <div className="p-b-1">
            <span className="muted">最近50次状态更改</span>
            <button className="btn btn-small btn-danger pull-right" onClick={this.clearHistory}>
              <i className="fa fa-trash" /> {` Clear history`}
            </button>
          </div>
        )}
        <ol className="alert-rule-list">
          {stateHistoryItems.length > 0 ? (
            stateHistoryItems.map((item, index) => {
              return (
                <li className="alert-rule-item" key={`${item.time}-${index}`}>
                  <div className={`alert-rule-item__icon ${item.stateModel.stateClass}`}>
                    <i className={item.stateModel.iconClass} />
                  </div>
                  <div className="alert-rule-item__body">
                    <div className="alert-rule-item__header">
                      <p className="alert-rule-item__name">{item.alertName}</p>
                      <div className="alert-rule-item__text">
                        <span className={`${item.stateModel.stateClass}`}>{item.stateModel.text}</span>
                      </div>
                    </div>
                    {item.info}
                  </div>
                  <div className="alert-rule-item__time">{item.time}</div>
                </li>
              );
            })
          ) : (
            <i>没有记录状态变化</i>
          )}
        </ol>
      </div>
    );
  }
}

export default StateHistory;
