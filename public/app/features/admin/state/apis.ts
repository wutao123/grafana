import { getBackendSrv } from '@grafana/runtime';

export interface ServerStat {
  name: string;
  value: number;
}

export const getServerStats = async (): Promise<ServerStat[]> => {
  try {
    const res = await getBackendSrv().get('api/admin/stats');
    return [
      { name: '用户总数', value: res.users },
      { name: '管理员总数', value: res.admins },
      { name: '编辑者总数', value: res.editors },
      { name: '观众总数', value: res.viewers },
      { name: '活动用户（最近30天）', value: res.activeUsers },
      { name: '活动管理员（最近30天）', value: res.activeAdmins },
      { name: '活跃编辑者（最近30天）', value: res.activeEditors },
      { name: '活跃观众（最近30天）', value: res.activeViewers },
      { name: '活动会话', value: res.activeSessions },
      { name: '仪表板总数', value: res.dashboards },
      { name: '组织总数', value: res.orgs },
      { name: '播放列表总数', value: res.playlists },
      { name: '快照总数', value: res.snapshots },
      { name: '仪表板标记总数', value: res.tags },
      { name: '标星仪表板总数', value: res.stars },
      { name: '告警总数', value: res.alerts },
    ];
  } catch (error) {
    console.error(error);
    throw error;
  }
};
