import React from 'react';
import { css } from 'emotion';
import { NavModel } from '@grafana/data';
import Page from '../../core/components/Page/Page';
import { LicenseChrome } from './LicenseChrome';
import { Forms } from '@grafana/ui';
import { hot } from 'react-hot-loader';
import { StoreState } from '../../types';
import { getNavModel } from '../../core/selectors/navModel';
import { connect } from 'react-redux';

interface Props {
  navModel: NavModel;
}

export const UpgradePage: React.FC<Props> = ({ navModel }) => {
  return (
    <Page navModel={navModel}>
      <Page.Contents>
        <UpgradeInfo
          editionNotice="您运行的是Grafana的开源版本。
          您必须安装企业版才能启用企业功能。"
        />
      </Page.Contents>
    </Page>
  );
};

const titleStyles = { fontWeight: 500, fontSize: '26px', lineHeight: '123%' };

interface UpgradeInfoProps {
  editionNotice?: string;
}

export const UpgradeInfo: React.FC<UpgradeInfoProps> = ({ editionNotice }) => {
  const columnStyles = css`
    display: grid;
    grid-template-columns: 100%;
    column-gap: 20px;
    row-gap: 40px;

    @media (min-width: 1050px) {
      grid-template-columns: 50% 50%;
    }
  `;

  return (
    <LicenseChrome header="Grafana企业版" subheader="免费试用" editionNotice={editionNotice}>
      <div className={columnStyles}>
        <FeatureInfo />
        <ServiceInfo />
      </div>
    </LicenseChrome>
  );
};

const GetEnterprise: React.FC = () => {
  return (
    <div style={{ marginTop: '40px', marginBottom: '30px' }}>
      <h2 style={titleStyles}>获得Grafana企业版</h2>
      <CallToAction />
      <p style={{ paddingTop: '12px' }}>
        您可以免费使用试用版 <strong>30天</strong>。<strong>试验期结束前5天</strong>，我们会提醒您。
      </p>
    </div>
  );
};

const CallToAction: React.FC = () => {
  return (
    <Forms.LinkButton
      variant="primary"
      size="lg"
      href="https://grafana.com/contact?about=grafana-enterprise&utm_source=grafana-upgrade-page"
    >
      联系我们获得免费试用
    </Forms.LinkButton>
  );
};

const ServiceInfo: React.FC = () => {
  return (
    <div>
      <h4>为您服务</h4>

      <List>
        <Item title="企业版插件" image="public/img/licensing/plugin_enterprise.svg" />
        <Item title="重要 SLA: 2 小时" image="public/img/licensing/sla.svg" />
        <Item title="无限制的专家支持" image="public/img/licensing/customer_support.svg">
          24x7x365 支持 via
          <List nested={true}>
            <Item title="Email" />
            <Item title="专用空闲渠道" />
            <Item title="Phone" />
          </List>
        </Item>
        <Item title="Hand-in-hand support" image="public/img/licensing/handinhand_support.svg">
          在升级过程中
        </Item>
      </List>

      <div style={{ marginTop: '20px' }}>
        <strong>也包括：</strong>
        <br />
        与Grafana实验室合作确定未来优先权的保障，并接受Grafana核心团队的培训。
      </div>
    </div>
  );
};

const FeatureInfo: React.FC = () => {
  return (
    <div style={{ paddingRight: '11px' }}>
      <h4>功能增强</h4>
      <FeatureListing />

      <GetEnterprise />
    </div>
  );
};

const FeatureListing: React.FC = () => {
  return (
    <List>
      <Item title="数据源权限" />
      <Item title="报告" />
      <Item title="SAML认证" />
      <Item title="增强LDAP集成" />
      <Item title="团队同步">LDAP, GitHub OAuth, Auth Proxy</Item>
      <Item title="贴牌" />
      <Item title="企业插件">
        <List nested={true}>
          <Item title="Oracle" />
          <Item title="Splunk" />
          <Item title="Service Now" />
          <Item title="Dynatrace" />
          <Item title="DataDog" />
          <Item title="AppDynamics" />
        </List>
      </Item>
    </List>
  );
};

interface ListProps {
  nested?: boolean;
}

const List: React.FC<ListProps> = ({ children, nested }) => {
  const listStyle = css`
    display: flex;
    flex-direction: column;
    padding-top: 8px;

    > div {
      margin-bottom: ${nested ? 0 : 8}px;
    }
  `;

  return <div className={listStyle}>{children}</div>;
};

interface ItemProps {
  title: string;
  image?: string;
}

const Item: React.FC<ItemProps> = ({ children, title, image }) => {
  const imageUrl = image ? image : 'public/img/licensing/checkmark.svg';
  const itemStyle = css`
    display: flex;

    > img {
      display: block;
      height: 22px;
      flex-grow: 0;
      padding-right: 12px;
    }
  `;
  const titleStyle = css`
    font-weight: 500;
    line-height: 1.7;
  `;

  return (
    <div className={itemStyle}>
      <img src={imageUrl} />
      <div>
        <div className={titleStyle}>{title}</div>
        {children}
      </div>
    </div>
  );
};

const mapStateToProps = (state: StoreState) => ({
  navModel: getNavModel(state.navIndex, 'upgrading'),
});

export default hot(module)(connect(mapStateToProps)(UpgradePage));
