import React, { PureComponent } from 'react';
import { dateTime } from '@grafana/data';
import { SyncInfo, UserDTO } from 'app/types';
import { Button, LinkButton } from '@grafana/ui';

interface Props {
  ldapSyncInfo: SyncInfo;
  user: UserDTO;
  onUserSync: () => void;
}

interface State {}

const syncTimeFormat = 'dddd YYYY-MM-DD HH:mm zz';
const debugLDAPMappingBaseURL = '/admin/ldap';

export class UserLdapSyncInfo extends PureComponent<Props, State> {
  onUserSync = () => {
    this.props.onUserSync();
  };

  render() {
    const { ldapSyncInfo, user } = this.props;
    const nextSyncTime = dateTime(ldapSyncInfo.nextSync).format(syncTimeFormat);
    const prevSyncSuccessful = ldapSyncInfo && ldapSyncInfo.prevSync;
    const prevSyncTime = prevSyncSuccessful ? dateTime(ldapSyncInfo.prevSync.started).format(syncTimeFormat) : '';
    const debugLDAPMappingURL = `${debugLDAPMappingBaseURL}?user=${user && user.login}`;

    return (
      <>
        <h3 className="page-heading">LDAP同步</h3>
        <div className="gf-form-group">
          <div className="gf-form">
            <table className="filter-table form-inline">
              <tbody>
                <tr>
                  <td>外部同步</td>
                  <td>用户通过LDAP同步-某些更改必须在LDAP或映射中完成。</td>
                  <td>
                    <span className="label label-tag">LDAP</span>
                  </td>
                </tr>
                <tr>
                  {ldapSyncInfo.enabled ? (
                    <>
                      <td>下一次预定同步</td>
                      <td colSpan={2}>{nextSyncTime}</td>
                    </>
                  ) : (
                    <>
                      <td>下一次预定同步</td>
                      <td colSpan={2}>未启用</td>
                    </>
                  )}
                </tr>
                <tr>
                  {prevSyncSuccessful ? (
                    <>
                      <td>上次同步</td>
                      <td>{prevSyncTime}</td>
                      <td>成功</td>
                    </>
                  ) : (
                    <td colSpan={3}>上次同步</td>
                  )}
                </tr>
              </tbody>
            </table>
          </div>
          <div className="gf-form-button-row">
            <Button variant="secondary" onClick={this.onUserSync}>
              同步用户
            </Button>
            <LinkButton variant="inverse" href={debugLDAPMappingURL}>
              调试LDAP映射
            </LinkButton>
          </div>
        </div>
      </>
    );
  }
}
