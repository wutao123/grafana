import React from 'react';
import { appEvents } from 'app/core/core';

export class HelpModal extends React.PureComponent {
  static tabIndex = 0;
  static shortcuts = {
    Global: [
      { keys: ['g', 'h'], description: '转到主仪表板' },
      { keys: ['g', 'p'], description: '转到配置文件' },
      { keys: ['s', 'o'], description: '打开搜索' },
      { keys: ['esc'], description: '退出编辑/设置视图' },
    ],
    Dashboard: [
      { keys: ['mod+s'], description: '保存仪表板' },
      { keys: ['d', 'r'], description: '刷新所有面板' },
      { keys: ['d', 's'], description: '仪表板设置' },
      { keys: ['d', 'v'], description: '在活动/查看模式下切换' },
      { keys: ['d', 'k'], description: '切换kiosk模式（隐藏顶部导航）' },
      { keys: ['d', 'E'], description: '展开所有行' },
      { keys: ['d', 'C'], description: '折叠所有行' },
      { keys: ['d', 'a'], description: '切换自动调整面板（实验功能）' },
      { keys: ['mod+o'], description: '切换共享图形十字线' },
      { keys: ['d', 'l'], description: '切换所有面板图例' },
    ],
    'Focused Panel': [
      { keys: ['e'], description: '切换面板编辑视图' },
      { keys: ['v'], description: '切换面板全屏视图' },
      { keys: ['p', 's'], description: '打开面板共享模式' },
      { keys: ['p', 'd'], description: '复制面板' },
      { keys: ['p', 'r'], description: '移除面板' },
      { keys: ['p', 'l'], description: '切换面板图例' },
    ],
    'Time Range': [
      { keys: ['t', 'z'], description: '缩小时间范围' },
      {
        keys: ['t', <i className="fa fa-long-arrow-left" />],
        description: '向后移动时间范围',
      },
      {
        keys: ['t', <i className="fa fa-long-arrow-right" />],
        description: '向前移动时间范围',
      },
    ],
  };

  dismiss() {
    appEvents.emit('hide-modal');
  }

  render() {
    return (
      <div className="modal-body">
        <div className="modal-header">
          <h2 className="modal-header-title">
            <i className="fa fa-keyboard-o" />
            <span className="p-l-1">快捷方式</span>
          </h2>
          <a className="modal-header-close" onClick={this.dismiss}>
            <i className="fa fa-remove" />
          </a>
        </div>

        <div className="modal-content help-modal">
          <p className="small" style={{ position: 'absolute', top: '13px', right: '44px' }}>
            <span className="shortcut-table-key">mod</span> =
            <span className="muted"> windows或linux上的CTRL和Mac上的CMD键</span>
          </p>

          {Object.entries(HelpModal.shortcuts).map(([category, shortcuts], i) => (
            <div className="shortcut-category" key={i}>
              <table className="shortcut-table">
                <tbody>
                  <tr>
                    <th className="shortcut-table-category-header" colSpan={2}>
                      {category}
                    </th>
                  </tr>
                  {shortcuts.map((shortcut, j) => (
                    <tr key={`${i}-${j}`}>
                      <td className="shortcut-table-keys">
                        {shortcut.keys.map((key, k) => (
                          <span className="shortcut-table-key" key={`${i}-${j}-${k}`}>
                            {key}
                          </span>
                        ))}
                      </td>
                      <td className="shortcut-table-description">{shortcut.description}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ))}

          <div className="clearfix" />
        </div>
      </div>
    );
  }
}
