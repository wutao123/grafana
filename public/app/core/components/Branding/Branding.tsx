import React, { FC } from 'react';
import { css, cx } from 'emotion';
import { config } from '@grafana/runtime';

export interface BrandComponentProps {
  className?: string;
  children?: JSX.Element | JSX.Element[];
}

export const LoginLogo: FC<BrandComponentProps> = ({ className }) => {
  const maxSize = css`
    max-width: 450px;
    width: 300px;
    height: auto;
  `;

  return (
    <>
      <img className={cx(className, maxSize)} src={config.bootData.branding.LoginLogo} alt="ebTelemetry" />
    </>
  );
};

export const LoginBackground: FC<BrandComponentProps> = ({ className, children }) => {
  const background = css`
    background: ${config.bootData.branding.LoginBackground};
    background-size: cover;
  `;

  return <div className={cx(background, className)}>{children}</div>;
};

export const MenuLogo: FC<BrandComponentProps> = ({ className }) => {
  return <img className={className} src={config.bootData.branding.MenuLogo} alt="ebTelemetry" />;
};

export const AppTitle = config.bootData.branding.AppTitle;

export class Branding {
  static LoginLogo = LoginLogo;
  static LoginBackground = LoginBackground;
  static MenuLogo = MenuLogo;
  static AppTitle = AppTitle;
}
